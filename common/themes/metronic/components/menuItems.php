<?php

namespace metronic\components;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use msoft\widgets\Icon;

class menuItems extends Component {

    public $key;
    public $action;
    public $appid;

    public function itemsAlias($key) {
        $items = [
            'Dashboard' => [
                '/site/index'
            ],
            'Admins' => [
                '/user/admin/index',
                '/admin/assignment/index',
                '/admin/route/index',
                '/admin/permission/index',
                '/admin/role/index',
                '/core/core-fields/index',
                '/core/core-item-alias/index',
                '/core/core-options/index',
                '/core/tables-fields/index',
                '/db/tables/index',
            ],
            'Meeting' => [
                '/meeting/meeting/index',
                '/meeting/meeting/create',
                '/meeting/meeting/update',
                '/meeting/meeting/meeting-note',
                '/meeting/meeting/meeting-list',
                '/meeting/meeting/certified', 
                '/meeting/meeting/meeting-view',
            ],
            'Invenroty' => [
                '/inventory/item/index',
                '/invenroty/receive/index',
                '/invenroty/request/index',
                '/invenroty/tranfer/index',
                '/invenroty/vendor/index',
            ],
            'Core' => [
                '/core/core-fields/index',
                '/core/core-item-alias/index',
                '/core/core-options/index',
                '/core/tables-fields/index'
            ],
            'Project' => [
                '/project/prj/index', '/project/prj/create', '/project/prj/update'
            ]
        ];
        return ArrayHelper::getValue($items, $key, []);
    }

    public function templateAlias($action, $key, $items = false) {
        //$bundle = Yii::$app->assetManager->getBundle('metronic\assets\MetronicAsset');
        if ($items == false) {
            return '<a href="{url}" class="nav-link nav-toggle"><i class="{icon}"></i> <span class="title">{label}</span></a>';
        } elseif (in_array($action, menuItems::itemsAlias($key)) === false) {
            return '<a href="{url}" class="nav-link nav-toggle"><i class="{icon}"></i> <span class="title">{label}</span><span class="arrow"></span></a>';
        } else {
            return '<a href="{url}" class="nav-link nav-toggle"><i class="{icon}"></i> <span class="title">{label}</span><span class="selected"></span><span class="arrow open"></span></a>';
        }
    }

    public function menuOptions() {
        return [
            'class' => 'page-sidebar-menu  page-header-fixed page-sidebar-menu-closed',
            'data-keep-expanded' => 'false',
            'data-auto-scroll' => 'true',
            'data-slide-speed' => 200,
            'style' => 'padding-top: 20px'
        ];
    }

    public function menuItemsAlias($appid, $action) {
        $options = ['class' => 'nav-item'];
        $heading = ['class' => 'heading'];
        $menus = [
            'app-frontend' => [
                ['label' => '<div class="sidebar-toggler"><span></span></div>', 'linkTemplate' => false, 'options' => ['class' => 'sidebar-toggler-wrapper hide']],
                [
                    'label' => 'Dashboard', 'url' => ['/site/index'], 'icon' => 'icon-home',
                    'options' => $options,
                    'template' => menuItems::templateAlias($action, 'Dashboard'),
                    /* 'items' => [
                      [
                      'label' => 'Dashboard 1','icon' => 'icon-bar-chart',
                      'url' => ['/site/index'],
                      'options' => $options
                      ],
                      [
                      'label' => 'Dashboard 2', 'icon' => 'icon-bulb',
                      'url' => ['/site/contact'],
                      'options' => $options
                      ],
                      ], */
                    'active' => in_array($action, menuItems::itemsAlias('Dashboard'))
                ],
                [
                    'label' => '<h3 class="uppercase">Meetings</h3>',
                    'options' => $heading,
                ],
                [
                    'label' => 'ระบบบันทึกการประชุม', 'url' => 'javascript:;', 'icon' => 'icon-note',
                    'options' => $options,
                    'template' => menuItems::templateAlias($action, 'Meeting', true),
                    'items' => [
                        [
                            'label' => 'รายการการประชุม', 'url' => ['/meeting/meeting/index'], 'icon' => 'icon-calendar',
                            'options' => $options,
                            'active' => in_array($action, [
                                '/meeting/meeting/index', '/meeting/meeting/create', '/meeting/meeting/update'
                            ]),
                        ],
                        [
                            'label' => 'บันทึกการประชุม', 'url' => ['/meeting/meeting/meeting-list'], 'icon' => 'icon-notebook',
                            'options' => $options,
                            'active' => in_array($action, [
                                '/meeting/meeting/meeting-list', '/meeting/meeting/meeting-note'
                            ]),
                        ],
                        [
                            'label' => 'ยืนยันผลการประชุม', 'url' => ['/meeting/meeting/certified'], 'icon' => 'icon-doc',
                            'options' => $options,
                            'active' => in_array($action, [
                                '/meeting/meeting/certified', '/meeting/meeting/meeting-view'
                            ]),
                        ],
                    ],
                    'active' => in_array($action, menuItems::itemsAlias('Meeting')),
                ],
                [
                    'label' => '<h3 class="uppercase"><i class="fa fa-cubes"></i> INVENTORY</h3>',
                    'options' => $heading,
                ],
                [
                    'label' => 'วัสดุสำนักงาน', 'url' => 'javascript:;', 'icon' => 'fa fa-cube',
                    'options' => $options,
                    'template' => menuItems::templateAlias($action, 'Invenroty', true),
                    'items' => [
                        [
                            'label' => 'รายการวัสดุสำนักงาน', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/inventory/item/index'],
                            'options' => $options,
                        ],
                        [
                            'label' => 'บันทึกรับ', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/inventory/receive/index?type=' . base64_encode('receive')],
                            'options' => $options,
                        ],
                        [
                            'label' => 'ตั้งต้นคลัง', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/inventory/receive/index?type=' . base64_encode('initial')],
                            'options' => $options,
                        ],
                        [
                            'label' => 'ขอเบิกวัสดุฯ', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/inventory/request/index?type=' . base64_encode('request')],
                            'options' => $options,
                        ],
                        [
                            'label' => 'อนุมัติเบิกวัสดุฯ', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/inventory/request/index?type=' . base64_encode('approve')],
                            'options' => $options,
                        ],
                        [
                            'label' => 'จ่ายวัสดุ', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/inventory/tranfer/index'],
                            'options' => $options,
                        ],
                        [
                            'label' => 'จัดการข้อมูลผู้ขาย', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/inventory/vendor/index'],
                            'options' => $options,
                        ],
                    ],
                    'active' => in_array($action, menuItems::itemsAlias('Invenroty')),
                ],
                [
                    'label' => 'ครุภัณฑ์สำนักงาน', 'url' => 'javascript:;', 'icon' => 'fa fa-cube',
                    'options' => $options,
                    'template' => menuItems::templateAlias($action, 'Invenroty', true),
                    'items' => [
                        [
                            'label' => 'รายการครุภัณฑ์', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/inventory/item/index'],
                            'options' => $options,
                        ],
                        [
                            'label' => 'บันทึกรับ', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/inventory/receive/index?type=' . base64_encode('receive')],
                            'options' => $options,
                        ],
                        [
                            'label' => 'บันทึกการซ่อม', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/'],
                            'options' => $options,
                        ],
                        [
                            'label' => 'บันทึกยืม-คืน', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/'],
                            'options' => $options,
                        ],
                    ],
                    'active' => in_array($action, menuItems::itemsAlias('Invenroty')),
                ],
                [
                    'label' => 'ครุภัณฑ์ห้องปฏิบัติการ', 'url' => 'javascript:;', 'icon' => 'fa fa-cube',
                    'options' => $options,
                    'template' => menuItems::templateAlias($action, 'Invenroty', true),
                    'items' => [
                        [
                            'label' => 'รายการครุภัณฑ์', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/inventory/item/index'],
                            'options' => $options,
                        ],
                        [
                            'label' => 'บันทึกรับ', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/inventory/receive/index?type=' . base64_encode('receive')],
                            'options' => $options,
                        ],
                        [
                            'label' => 'บันทึกการซ่อม', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/'],
                            'options' => $options,
                        ],
                        [
                            'label' => 'บันทึกยืม-คืน', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/'],
                            'options' => $options,
                        ],
                    ],
                    'active' => in_array($action, menuItems::itemsAlias('Invenroty')),
                ],
                [
                    'label' => '<h3 class="uppercase">Project Management</h3>',
                    'options' => $heading,
                ],
                [
                    'label' => 'ระบบบริหารจัดการโครงการ', 'url' => 'javascript:;', 'icon' => 'icon-directions',
                    'options' => $options,
                    'template' => menuItems::templateAlias($action, 'Project', true),
                    'items' => [
                        [
                            'label' => 'DASHBOARD', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/'],
                            'options' => $options,
                        ],
                        [
                            'label' => 'ข้อมูลโครงการ', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/project/prj/index'],
                            'options' => $options,
                            'active' => in_array($action, [
                                '/project/prj/index', '/project/prj/create', '/project/prj/update'
                            ]),
                        ],
                        [
                            'label' => 'ผลการดำเนินการ', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/'],
                            'options' => $options,
                        ],
                        [
                            'label' => 'เบิก-จ่ายโครงการ', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/'],
                            'options' => $options,
                        ],
                        [
                            'label' => 'ตั้งค่า', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/'],
                            'options' => $options,
                        ],
                    ],
                    'active' => in_array($action, menuItems::itemsAlias('Project')),
                ],
                [
                    'label' => '<h3 class="uppercase">ระบบจองห้องประชุม</h3>',
                    'options' => $heading,
                ],
                [
                    'label' => 'ตารางการใช้ห้องประชุม','icon' => 'icon-calendar',
                    'url' => ['/rooming/events/rooms-table'],
                    'options' => $options,
                    'active' => in_array($action,[
                        '/rooming/events/rooms-table'
                    ]),
                ],
                [
                    'label' => 'ตั้งค่า','icon' => 'icon-settings',
                    'url' => ['/rooming/events/room-detail'],
                    'options' => $options,
                    'active' => in_array($action,[
                        '/rooming/events/room-detail'
                    ]),
                ],
                /*[
                    'label' => 'Modules', 'url' => 'javascript:;', 'icon' => 'icon-settings', 
                    'options' => $options,
                    'template' => menuItems::templateAlias($action,'Admins',true),
                    'items' => [
                        [
                            'label' => 'Users','icon' => 'icon-users',
                            'url' => ['/user/admin/index'],
                            'options' => $options,
                        ],
                        [
                            'label' => 'Permissions', 'url' => 'javascript:;', 'icon' => 'icon-users', 
                            'options' => $options,
                            'template' => menuItems::templateAlias($action,'Permissions',true),
                            'items' => [
                                [
                                    'label' => 'Assignment','icon' => 'fa fa-circle-thin',
                                    'url' => ['/admin/assignment/index'],
                                    'options' => $options,
                                ],
                                [
                                    'label' => 'Role', 'icon' => 'fa fa-circle-thin',
                                    'url' => ['/admin/role/index'],
                                    'options' => $options,
                                ],
                                [
                                    'label' => 'Permission', 'icon' => 'fa fa-circle-thin',
                                    'url' => ['/admin/permission/index'],
                                    'options' => $options,
                                ],
                                [
                                    'label' => 'Route', 'icon' => 'fa fa-circle-thin',
                                    'url' => ['/admin/route/index'],
                                    'options' => $options,
                                ],
                                
                            ],
                            'active' => in_array($action,menuItems::itemsAlias('Permissions'))
                        ],
                        // [
                        //     'label' => 'Clear Cache','icon' => 'fa fa-circle-thin',
                        //     'url' => ['/site/clear-cache'],
                        //     'options' => $options,
                        // ],
                    ],
                    'active' => in_array($action,menuItems::itemsAlias('Admins'))
                ],
                [
                    'label' => 'Gii','icon' => 'icon-wrench',
                    'url' => ['/gii'],
                    'options' => $options,
                ],
                [
                    'label' => 'Report Settings','icon' => 'icon-bar-chart',
                    'url' => ['/report/default/index'],
                    'options' => $options,
                ],
                [
                    'label' => '<h3 class="uppercase">Helpers</h3>',
                    'options' => $heading,
                ],
                [
                    'label' => 'Guide Helpers','icon' => 'icon-grid',
                    'url' => ['/site/widgets-theme'],
                    'options' => $options,
                ],
                [
                    'label' => 'App Config','icon' => 'icon-wrench',
                    'url' => ['/site/config'],
                    'options' => $options,
                ],
                [
                    'label' => 'Demo Ajax Form','icon' => 'icon-tag',
                    'url' => ['/tb-schemas/index'],
                    'options' => $options,
                ],
                [
                    'label' => 'Generate Javascript','icon' => 'icon-calendar',
                    'url' => ['/generate/default/gii'],
                    'options' => $options,
                ],*/
            ],
            'app-backend' => [
                ['label' => '<div class="sidebar-toggler"><span></span></div>', 'linkTemplate' => false, 'options' => ['class' => 'sidebar-toggler-wrapper hide']],
                [
                    'label' => '<h3 class="uppercase">Backend Admins</h3>',
                    'options' => $heading,
                ],
                [
                    'label' => 'Modules', 'url' => 'javascript:;', 'icon' => 'icon-settings',
                    'options' => $options,
                    'template' => menuItems::templateAlias($action, 'Admins', true),
                    'items' => [
                        /* [
                          'label' => 'Users','icon' => 'icon-users',
                          'url' => ['/user/admin/index'],
                          'options' => $options,
                          ],
                          [
                          'label' => 'Permissions', 'url' => 'javascript:;', 'icon' => 'icon-users',
                          'options' => $options,
                          'template' => menuItems::templateAlias($action,'Permissions',true),
                          'items' => [
                          [
                          'label' => 'Assignment','icon' => 'fa fa-circle-thin',
                          'url' => ['/admin/assignment/index'],
                          'options' => $options,
                          ],
                          [
                          'label' => 'Role', 'icon' => 'fa fa-circle-thin',
                          'url' => ['/admin/role/index'],
                          'options' => $options,
                          ],
                          [
                          'label' => 'Permission', 'icon' => 'fa fa-circle-thin',
                          'url' => ['/admin/permission/index'],
                          'options' => $options,
                          ],
                          [
                          'label' => 'Route', 'icon' => 'fa fa-circle-thin',
                          'url' => ['/admin/route/index'],
                          'options' => $options,
                          ],

                          ],
                          'active' => in_array($action,menuItems::itemsAlias('Permissions'))
                          ], */
                        [
                            'label' => 'Core', 'url' => 'javascript:;', 'icon' => 'icon-puzzle',
                            'options' => $options,
                            'template' => menuItems::templateAlias($action, 'Core', true),
                            'items' => [
                                [
                                    'label' => 'CoreFields', 'icon' => 'fa fa-circle-thin',
                                    'url' => ['/core/core-fields/index'],
                                    'options' => $options,
                                ],
                                [
                                    'label' => 'CoreItemAlias', 'icon' => 'fa fa-circle-thin',
                                    'url' => ['/core/core-item-alias/index'],
                                    'options' => $options,
                                ],
                                [
                                    'label' => 'CoreOptions', 'icon' => 'fa fa-circle-thin',
                                    'url' => ['/core/core-options/index'],
                                    'options' => $options,
                                ],
                                [
                                    'label' => 'TablesFields', 'icon' => 'fa fa-circle-thin',
                                    'url' => ['/core/tables-fields/index'],
                                    'options' => $options,
                                ],
                            ],
                            'active' => in_array($action, menuItems::itemsAlias('Core'))
                        ],
                        [
                            'label' => 'DB MANAGE', 'icon' => 'fa fa-database',
                            'url' => ['/db/tables/index'],
                            'options' => $options,
                        ],
                        [
                            'label' => 'Clear Cache', 'icon' => 'fa fa-circle-thin',
                            'url' => ['/site/clear-cache'],
                            'options' => $options,
                        ],
                    ],
                    'active' => in_array($action, menuItems::itemsAlias('Admins'))
                ],
            ]
        ];
        return ArrayHelper::getValue($menus, $appid, []);
    }

}

?>