<?php
namespace metronic\widgets\panel;

use yii\web\AssetBundle;


class PanelWidgetAsset extends AssetBundle
{
    public $sourcePath = '@metronic/widgets/panel/assets';
//    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
//        'panel-widget.css',
    ];
    public $js = [
        'js/jquery.panel-widget.js?v1.0',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}