<?php
/**
 * @copyright Copyright (c) 2013-16 2amigOS! Consulting Group LLC
 * @link http://2amigos.us
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
namespace metronic\widgets\ckeditor;

use yii\web\AssetBundle;

/**
 * CKEditorAsset
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @package dosamigos\ckeditor
 */
class CKEditorAsset extends AssetBundle
{
    public $sourcePath = '@metronic/widgets/ckeditor/assets';
    public $js = [
        'ckeditor/ckeditor.js',
        'ckeditor/adapters/jquery.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];
}