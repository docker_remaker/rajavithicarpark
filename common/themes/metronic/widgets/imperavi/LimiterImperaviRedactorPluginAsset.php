<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace metronic\widgets\imperavi;
use yii\web\AssetBundle;

/**
 * @author Alexander Yaremchuk <alwex10@gmail.com>
 * @since 1.5
 */
class LimiterImperaviRedactorPluginAsset extends AssetBundle
{
    public $sourcePath = '@metronic/widgets/imperavi/assets/plugins/limiter';
    public $js = [
        'limiter.js',
    ];
    public $css = [

    ];
    public $depends = [
        'metronic\widgets\imperavi\ImperaviRedactorAsset'
    ];
}