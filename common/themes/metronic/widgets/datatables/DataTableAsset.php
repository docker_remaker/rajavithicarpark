<?php
/**
 * @copyright Copyright (c) 2014 Serhiy Vinichuk
 * @license MIT
 * @author Serhiy Vinichuk <serhiyvinichuk@gmail.com>
 */

namespace metronic\widgets\datatables;


use yii\web\AssetBundle;

class DataTableAsset extends AssetBundle
{
    const STYLING_DEFAULT = 'default';
    const STYLING_BOOTSTRAP = 'bootstrap';
    const STYLING_MATERIAL = 'material';
    const STYLING_JUI = 'jqueryui';

    public $styling = self::STYLING_DEFAULT;
    public $fontAwesome = false;
    public $sourcePath = '@metronic/assets';

    public $cssOptions = ['position' => \yii\web\View::POS_HEAD];
    public $jsOptions = ['position' => \yii\web\View::POS_END];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\grid\GridViewAsset'
    ];

    public function init()
    {
        parent::init();

        // if (empty($this->js)) {
        //     $this->js = ['global/scripts/datatable.js' . (YII_ENV_DEV ? '' : '.min') . '.js'];
        // }
        switch ($this->styling) {
            case self::STYLING_JUI:
                $this->depends[] = 'yii\jui\JuiAsset';
                $this->css[] = 'datatables-plugins/integration/jqueryui/dataTables.jqueryui.css';
                $this->js[] = 'datatables-plugins/integration/jqueryui/dataTables.jqueryui.min.js';
                break;
            case self::STYLING_BOOTSTRAP:
                $this->depends[] = 'yii\bootstrap\BootstrapAsset';
                $this->css[] = 'global/plugins/datatables/datatables.min.css';
                $this->css[] = 'global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css';
                $this->css[] = 'global/plugins/datatables/Responsive/css/responsive.bootstrap.min.css';
                $this->js[] = 'global/scripts/datatable.js';
                $this->js[] = 'global/plugins/datatables/datatables.min.js';
                $this->js[] = 'global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js';
                $this->js[] = 'global/plugins/datatables/Responsive/js/responsive.bootstrap.min.js';
                break;
            case self::STYLING_MATERIAL:
                $this->depends[] = 'yii\bootstrap\BootstrapAsset';
                $this->css[] = 'global/plugins/datatables/dataTables.min.css';
                $this->css[] = 'global/plugins/datatables/dataTables.material.min.css';
                $this->css[] = 'global/plugins/datatables/material.min.css';
                $this->js[] = 'global/scripts/datatable.js';
                $this->js[] = 'global/plugins/datatables/datatables.min.js';
                $this->js[] = 'global/plugins/datatables/dataTables.material.min.js';
                break;
            case self::STYLING_DEFAULT:
                $this->css[] = 'global/scripts/datatable.js' . (YII_ENV_DEV ? '' : '.min') . '.css';
                break;
            default;
        }

        if ($this->fontAwesome) {
            $this->css[] = 'dataTables.fontAwesome.css';
        }
    }

} 