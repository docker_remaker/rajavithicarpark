<?php

namespace metronic\widgets\flot;

use Yii;
use yii\base\Model;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\base\Widget as Widget;

class FlotChart extends Widget
{

	public $options = [];

	public $data = [];

	public $htmlOptions = [];

	public $plugins = [];

	public $excanvas = false;

	public $tagName = 'div';

	/**
	 * Initializes the widget.
	 * If you override this method, make sure you call the parent implementation first.
	 */
	public function init()
	{
		//checks for the element id
		if (!isset($this->htmlOptions['id'])) {
			$this->htmlOptions['id'] = $this->getId();
		}
		parent::init();
	}

	/**
	 * Renders the widget.
	 */
	public function run()
	{
		$this->registerPlugin();
		echo Html::tag($this->tagName, '', $this->htmlOptions);
	}

	protected function registerPlugin()
	{
		$id   = $this->htmlOptions['id'];
		$view = $this->getView();

		if ($this->excanvas) {
			FlotChartAsset::$extra_js[] = defined('YII_DEBUG') && YII_DEBUG ? 'excanvas.js' : 'excanvas.min.js';
		}

		if ($this->plugins && is_array($this->plugins)) {
			foreach ($this->plugins as $plugin_code) {
				$plugin_jsname = Plugin::getFilename($plugin_code);
				if ($plugin_jsname) {
					FlotChartAsset::$extra_js[] = $plugin_jsname;
				}
			}
		}

		FlotChartAsset::register($view);

		$flotdata    = Json::encode($this->data);
		$flotoptions = Json::encode($this->options);
		$placeholder = "$('#${id}')";

		$js   = [];
		$js[] = "$.plot({$placeholder}, {$flotdata}, {$flotoptions});";
		$view->registerJs(implode("\n", $js),View::POS_READY);
	}
}

