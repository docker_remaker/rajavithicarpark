<?php

namespace metronic\widgets\bstable;
use yii\web\AssetBundle;

/**
 * Asset for the DataTables JQuery plugin
 * @author Federico Nicolás Motta <fedemotta@gmail.com>
 */
class BootstrapTableAsset extends AssetBundle 
{
    public $sourcePath = '@metronic/assets/global/plugins/bootstrap-table'; 
    public $css = [
        "bootstrap-table.min.css",
    ];

    public $js = [
        "bootstrap-table.min.js",
        "bootstrap-table-locale-all.min.js",
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}