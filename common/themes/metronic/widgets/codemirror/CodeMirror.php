<?php
namespace metronic\widgets\codemirror;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;
use metronic\assets\CodeMirrorAsset;

class CodeMirror extends InputWidget
{
    public $clientOptions = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->hasModel()) {
            echo Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textarea($this->name, $this->value, $this->options);
        }
        $this->registerPlugin();
    }

    /**
     * Registers CodeMirror plugin
     * @codeCoverageIgnore
     */
    protected function registerPlugin()
    {
        $js = [];

        $view = $this->getView();

        CodeMirrorAsset::register($view);

        $id = $this->options['id'];

        $options = $this->clientOptions !== false && !empty($this->clientOptions)
            ? Json::encode($this->clientOptions)
            : '{}';
        $js[] = "var myTextArea".$id." = document.getElementById('$id');";
        $js[] = "var myCodeMirror = CodeMirror.fromTextArea(myTextArea".$id.",$options);";

        $view->registerJs(implode("\n", $js));
    }
}