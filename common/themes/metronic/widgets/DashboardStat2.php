<?php
namespace metronic\widgets;

use msoft\widgets\Icon;
use yii\bootstrap\Widget;
use yii\helpers\Html;

class DashboardStat2 extends Widget
{
    /**
     * @var string small box background color
     */
    public $fontcolor = 'font-green-sharp';

    /**
     * @var string font awesome icon name
     */
    public $icon;

    /**
     * @var string header text
     */
    public $desc;


    public $text;

    /**
     * @var string value number
     */
    public $number;

    public $progresscolor = 'green-sharp';

    public $width;

    public $title;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        Html::addCssClass($this->options, 'dashboard-stat2 ');
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo Html::beginTag('div', $this->options).' '.
             Html::beginTag('div',['class' => 'display']).' '.
             Html::beginTag('div', ['class' => 'number']).' '.
             Html::beginTag('h3', ['class' => $this->fontcolor]).' '.
             Html::tag('span', $this->number, ['data-counter' => 'counterup','data-value' => $this->number]).' '.
             Html::tag('small', $this->text, ['class' => $this->fontcolor]).' '.
             Html::endTag('h3').' '.
             Html::tag('small', $this->desc, []).' '.
             Html::endTag('div');
        if (!empty($this->icon)) {
            echo Html::tag('div', Icon::show($this->icon), ['class' => 'icon']);
        }
        echo Html::endTag('div').' '.
             Html::beginTag('div', ['class' => 'progress-info']).' '.
             Html::beginTag('div', ['class' => 'progress']).' '.
             Html::tag('span', '', ['class' => 'progress-bar progress-bar-success '.$this->progresscolor,'style' => 'width:'.$this->width.'%']).' '.
             Html::tag('span', $this->width.'% '.$this->title, ['class' => 'sr-only']).' '.
             Html::endTag('div').' '.
             Html::beginTag('div',['class' => 'status']).' '.
             Html::tag('div', $this->title, ['class' => 'status-title']).' '.
             Html::tag('div', $this->width.'%', ['class' => 'status-number']).' '.
             Html::endTag('div').' '.
             Html::endTag('div').' '.
             Html::endTag('div');
    }
}