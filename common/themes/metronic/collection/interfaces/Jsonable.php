<?php

namespace metronic\collection\interfaces;

/**
 * Interface Jsonable
 *
 * @package yii2mod\collection\interfaces
 */
interface Jsonable
{
    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     *
     * @return string
     */
    public function toJson($options = 0);
}
