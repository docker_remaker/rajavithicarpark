<?php
namespace metronic\classes;

use Yii;
use msoft\utils\CoreUtility;
use yii\helpers\ArrayHelper;
use metronic\helpers\Html;
use metronic\user\models\TbUserTitlename;
use metronic\user\models\TbSubject;
use metronic\user\models\TbFaculty;
use metronic\user\models\TbDepartment;
use metronic\user\models\TbCourse;
use metronic\user\models\TbAcademicposition;
use metronic\user\models\Nationalrity;
use metronic\user\models\Race;
use metronic\user\models\Religion;
use metronic\user\models\Province;
use metronic\user\models\Zipcode;
use metronic\user\models\TbMilitaryStatus;
use metronic\user\models\TbUserPosition;

class AppFunc {

    public static function generateInput($option, $model, $form, $field_name = 'option_name') {
        $options = ArrayHelper::merge(CoreUtility::string2Array($option['field_meta']), CoreUtility::string2Array($option['input_meta']));
		$specific = CoreUtility::string2Array($option['input_specific']);
    }

    public static function getUserTitleName(){
        return ArrayHelper::map(TbUserTitlename::find()->asArray()->all(),'user_titlename_id','user_titlename');
    }

    public static function getSubject(){
        return ArrayHelper::map(TbSubject::find()->asArray()->all(),'subject_id','subject_name');
    }

    public static function getFaculty(){
        return ArrayHelper::map(TbFaculty::find()->asArray()->all(),'faculty_id','faculty_name');
    }

    public static function getDepartment(){
        return ArrayHelper::map(TbDepartment::find()->asArray()->all(),'department_id','department_name');
    }

    public static function getCourseList(){
        return ArrayHelper::map(TbCourse::find()->asArray()->all(),'course_id','course_name');
    }

    public static function getAcademicposition(){
        return ArrayHelper::map(TbAcademicposition::find()->asArray()->all(),'user_academicpositionid','user_academicposition');
    }

    public static function getNationalrity(){
        return ArrayHelper::map(Nationalrity::find()->asArray()->all(),'NATIONALRITYID','NATIONALRITY_NAME');
    }

    public static function getRace(){
        return ArrayHelper::map(Race::find()->asArray()->all(),'RACEID','RACE_NAME');
    }

    public static function getReligion(){
        return ArrayHelper::map(Religion::find()->asArray()->all(),'RELIGIONID','RELIGION_NAME');
    }

    public static function getProvince(){
        return ArrayHelper::map(Province::find()->asArray()->all(),'PROVINCE_ID','PROVINCE_NAME');
    }

    public function getZipcodeList() {
        $query = Zipcode::find()->asArray()->all();
        return ArrayHelper::getColumn($query,'ZIPCODE');
    }

    public static function getMilitaryStatus(){
        return ArrayHelper::map(TbMilitaryStatus::find()->asArray()->all(),'military_status_id','military_status_name');
    }

    public static function getUserPosition(){
        return ArrayHelper::map(TbUserPosition::find()->asArray()->all(),'user_position_id','user_position');
    }
}
?>