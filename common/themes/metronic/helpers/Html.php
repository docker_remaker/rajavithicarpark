<?php 
namespace metronic\helpers;

use Yii;
use yii\helpers\BaseHtml;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;
use yii\helpers\Json;
use metronic\widgets\datatables\DataTableAsset;

class Html extends BaseHtml {
    const TYPE_DF = 'btn btn-default';
    const TYPE_PM = 'btn btn-primary';
    const TYPE_SS = 'btn btn-success';
    const TYPE_INFO = 'btn btn-info';
    const TYPE_WN = 'btn btn-warning';
    const TYPE_DG = 'btn btn-danger';

    const LARGE = 'btn-lg';
    const SMALL = 'btn-sm';
    const EXTRA_SMALL = 'btn-xs';

    #default
    const DF = 'btn btn-default';
    const PM = 'btn btn-primary';
    const SS = 'btn btn-success';
    const INF = 'btn btn-info';
    const WN = 'btn btn-warning';
    const DG = 'btn btn-danger';
    #large
    const DF_LG = 'btn btn-default '.self::LARGE;
    const PM_LG = 'btn btn-primary '.self::LARGE;
    const SS_LG = 'btn btn-success '.self::LARGE;
    const INF_LG = 'btn btn-info '.self::LARGE;
    const WN_LG = 'btn btn-warning '.self::LARGE;
    const DG_LG = 'btn btn-danger '.self::LARGE;
    #small
    const DF_SM = 'btn btn-default '.self::SMALL;
    const PM_SM = 'btn btn-primary '.self::SMALL;
    const SS_SM = 'btn btn-success '.self::SMALL;
    const INF_SM = 'btn btn-info '.self::SMALL;
    const WN_SM = 'btn btn-warning '.self::SMALL;
    const DG_SM = 'btn btn-danger '.self::SMALL;

    #extra small
    const DF_XS = 'btn btn-default '.self::EXTRA_SMALL;
    const PM_XS = 'btn btn-primary '.self::EXTRA_SMALL;
    const SS_XS = 'btn btn-success '.self::EXTRA_SMALL;
    const INF_XS = 'btn btn-info '.self::EXTRA_SMALL;
    const WN_XS = 'btn btn-warning '.self::EXTRA_SMALL;
    const DG_XS = 'btn btn-danger '.self::EXTRA_SMALL;

    const OUTLINE = 'btn-outline';

    //const DEFAULT = 'btn default';
    const RED = 'btn red';
    const BLUE = 'btn blue';
    const GREEN = 'btn green';
    const YELLOW = 'btn yellow';
    const PURPLE = 'btn purple';
    const DARK = 'btn dark';

    const DEFAULT_LG = 'btn default '.self::LARGE;
    const RED_LG = 'btn red '.self::LARGE;
    const BLUE_LG = 'btn blue '.self::LARGE;
    const GREEN_LG = 'btn green '.self::LARGE;
    const YELLOW_LG = 'btn yellow '.self::LARGE;
    const PURPLE_LG = 'btn purple '.self::LARGE;
    const DARK_LG = 'btn dark '.self::LARGE;

    const DEFAULT_SM = 'btn default '.self::SMALL;
    const RED_SM = 'btn red '.self::SMALL;
    const BLUE_SM = 'btn blue '.self::SMALL;
    const GREEN_SM = 'btn green '.self::SMALL;
    const YELLOW_SM = 'btn yellow '.self::SMALL;
    const PURPLE_SM = 'btn purple '.self::SMALL;
    const DARK_SM = 'btn dark '.self::SMALL;

    const DEFAULT_XS = 'btn default '.self::EXTRA_SMALL;
    const RED_XS = 'btn red '.self::EXTRA_SMALL;
    const BLUE_XS = 'btn blue '.self::EXTRA_SMALL;
    const GREEN_XS = 'btn green '.self::EXTRA_SMALL;
    const YELLOW_XS = 'btn yellow '.self::EXTRA_SMALL;
    const PURPLE_XS = 'btn purple '.self::EXTRA_SMALL;
    const DARK_XS = 'btn dark '.self::EXTRA_SMALL;

    const DEL = '<i class="fa fa-trash-o"></i> Delete';
    const EDIT = '<i class="fa fa fa-edit"></i> Edit';

    public static function aOutline($text, $url = null, $options = [] ){
        if ($url !== null) {
            $options['href'] = Url::to($url);
        }
        $options['class'] = $options['class'].' '.self::OUTLINE;
        return static::tag('a', $text, $options);
    }

    public static function btnOutline($content = 'Button', $options = [])
    {
        if (!isset($options['type'])) {
            $options['type'] = 'button';
        }
        $options['class'] = $options['class'].' '.self::OUTLINE;
        return static::tag('button', $content, $options);
    }

    public static function table($options = [],$view = '')
    {
        if (isset($options['datatables'])) {
            DataTableAsset::register($view);
            $id = $options['id'];
            if (isset($options['datatableOptions'])) {
                $datatableOptions = ArrayHelper::remove($options,'datatableOptions');
                $datatableOptions = Json::encode($datatableOptions);
                $js = "$('#{$id}').DataTable({$datatableOptions});";
            }else{
                $js = "$('#{$id}').DataTable();";
            }
            $view->registerJs($js, $view::POS_END);
        }
        return static::beginTag('table', $options);
    }

    public static function generateTableBody($data){
        if (empty($data)) {
            return '';
        }
        if (is_string($data)) {
            return $data;
        }
        $rows = '';
        if (is_array($data)) {
            $rowOptions = ArrayHelper::getValue($data, 'options', []);
            $columns = ArrayHelper::getValue($data, 'columns', []);
            $rows .= Html::beginTag('tr', $rowOptions);
            foreach ($columns as $col) {
                $colOptions = isset($col['options']) ? $col['options'] : [];
                $colContent = isset($col['content']) ? $col['content'] : [];
                if (empty($col)) {
                    continue;
                }
                
                $rows .= "\t" . Html::tag('td', $colContent, $colOptions) . "\n";
            }
            $rows .= Html::endTag('tr') . "\n";
        }
        return $rows;
    }

    public static function generateTableHeader($data){
        if (empty($data)) {
            return '';
        }
        if (is_string($data)) {
            return $data;
        }
        $rows = '';
        if (is_array($data)) {
            $rowOptions = ArrayHelper::getValue($data, 'options', []);
            $columns = ArrayHelper::getValue($data, 'columns', []);
            $rows .= Html::beginTag('thead', []);
            $rows .= Html::beginTag('tr', $rowOptions);
            foreach ($columns as $col) {
                if (empty($col)) {
                    continue;
                }
                $colOptions = isset($col['options']) ? $col['options'] : [];
                $colContent = isset($col['content']) ? $col['content'] : [];
                $rows .= Html::tag('th', $colContent, $colOptions) . "\n";
            }
            $rows .= Html::endTag('tr') . "\n";
            $rows .= Html::endTag('thead') . "\n";
        }
        return $rows;
    }
    
}
#================================ Exmple Code Table ==================================
/*
<?= Html::table([
    'class' => 'table table-striped table-hover',
    // 'id' => 'table',
    // 'datatables' => true,
    // 'datatableOptions' => [
    //     'responsive' => true,
    //     'info' => false
    // ]
],$this) ?>
    <?= Html::generateTableHeader([
        'options' => [],
        'columns' => [
            [
                'content' => 'id',
                'options' => ['style' => 'text-align:center;']
            ],
            [
                'content' => 'name',
                'options' => []
            ],
            [
                'content' => 'primaryKey',
                'options' => []
            ],
            [
                'content' => 'Action',
                'options' => ['style' => 'text-align:center;']
            ],
        ]
    ]); ?>
    <?php foreach($query as $key => $value): ?>
        <?= Html::generateTableBody([
            'options' => [],
            'columns' => [
                [
                    'content' => $value['id'],
                    'options' => ['style' => 'text-align:center;']
                ],
                [
                    'content' => $value['name'],
                    'options' => []
                ],
                [
                    'content' => $value['primaryKey'],
                    'options' => []
                ],
                [
                    'content' => Html::aOutline(Html::EDIT,false,['class' => Html::DARK_SM,'title' => 'Edit']).' '.
                                Html::aOutline(Html::DEL,false,['class' => Html::RED_SM]),
                    'options' => ['style' => 'text-align:center;']
                ],
            ]
        ]); ?>
    <?php endforeach; ?>
    
<?= Html::endTag('table') ?>