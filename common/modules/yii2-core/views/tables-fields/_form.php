<?php

use msoft\widgets\ActiveForm;
use msoft\helpers\Noty;
use yii\helpers\ArrayHelper;
use msoft\core\models\CoreFields;
use msoft\core\classes\CoreFunc;
use msoft\helpers\Html;
use msoft\widgets\Typeahead;
use msoft\widgets\Select2;
use msoft\core\assets\CoreAsset;
use msoft\helpers\RegisterJS;
use msoft\widgets\CodeMirror;
use msoft\widgets\TouchSpin;
use msoft\widgets\CheckboxX;
use msoft\core\classes\CoreQuery;
use msoft\core\models\TablesFields;

use yii\bootstrap\Modal;

RegisterJS::regis(['sweetalert','ajaxcrud'],$this);
CoreAsset::register($this);
$schema = Yii::$app->db->schema;

$query = TablesFields::find()->all();
?>
<style>
.CodeMirror {
    border: 1px solid #eee;
    height: auto;
}
</style>
<div class="tables-fields-form">

    <?php $form = ActiveForm::begin([
        'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="itemModalLabel">Tables Fields</h4>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <?php
                    echo $form->field($model, 'table_name')->widget(Typeahead::classname(), [
                        'options' => ['placeholder' => 'Filter table name ...'],
                        'pluginOptions' => ['highlight' => true, 'minLength' => 0],
                        'readonly'=>(isset($_GET['id'])),
                        'dataset' => [
                            [
                                    'local' => $schema->getTablenames(),
                                    'limit' => 10
                            ]
                        ]
                    ]);
                ?>
            </div>
            <div class="col-md-6">
                <?=
                    $form->field($model, 'action_id')->widget(Typeahead::classname(), [
                        'options' => ['placeholder' => 'Filter Action...'],
                        'pluginOptions' => ['highlight' => true, 'minLength' => 0],
                        'dataset' => [
                                [
                                        'local' => ArrayHelper::getColumn($query,'action_id'),
                                        'limit' => 20
                                ]
                        ]
                    ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'table_varname')->textInput(['maxlength' => true, 'readonly'=>(isset($_GET['id']))]) ?>
            </div>
            <div class="col-md-3 sdbox-col">
                <?= (isset($_GET['id']))?$form->field($model, 'table_field_type')->textInput(['readonly'=>(isset($_GET['id']))]):$form->field($model, 'table_field_type')->dropDownList(CoreFunc::itemAlias('field_type')) ?>
            </div>
            <div class="col-md-2 sdbox-col">
                <?= $form->field($model, 'table_length')->textInput(['maxlength' => true, 'readonly'=>(isset($_GET['id']))]) ?>
            </div>
            <div class="col-md-2 sdbox-col">
                <?= $form->field($model, 'table_default')->textInput(['readonly'=>(isset($_GET['id']))]) ?>
            </div>
            <div class="col-md-2 sdbox-col">
                <?= $form->field($model, 'table_index')->dropDownList(CoreFunc::itemAlias('field_index'), ['prompt'=>'None', 'readonly'=>(isset($_GET['id']))]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
            <?=
                $form->field($model, 'input_field')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(CoreFields::find()->all(), 'field_code', 'field_code'),
                        'options' => ['placeholder' => 'Select field ...'],
                        'pluginOptions' => [
                                'allowClear' => true
                        ],
                ]);
            ?>
            </div>
            <div class="col-md-3 sdbox-col">
                <?= $form->field($model, 'input_label')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3 sdbox-col" style="padding-top: 21px;">
                <?= $form->field($model, 'input_required',['showLabels'=>false])->widget(CheckboxX::classname(), [
                    'pluginOptions'=>['threeState'=>false],
                    'labelSettings' => [
                        'label' => $model->getAttributeLabel('input_required'),
                        'position' => CheckboxX::LABEL_RIGHT
                    ],
                    'options'=>['value' => !empty($model['input_required']) ? 1 : 0]
                    
                ]);  ?>
                <?php // $form->field($model, 'input_required')->checkbox() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'input_hint')->widget(CodeMirror::classname(), [
                    'options' => [
                        'id' => 'input_hint'
                    ],
                    'clientOptions' => [
                        'lineNumbers' => true,
                        'matchBrackets' => true,
                        'styleActiveLine' => true,
                        'theme' => 'ambiance',
                        'mode' => 'text/x-php',
                    ],
                ]);?>
                <?php // $form->field($model, 'input_hint')->textarea(['rows' => 3]) ?>
            </div>
            <div class="col-md-6 sdbox-col">
                <?= $form->field($model, 'input_data')->widget(CodeMirror::classname(), [
                    'options' => [
                        'id' => 'input_data',
                        'placeholder'=>'String array or function'
                    ],
                    'clientOptions' => [
                        'lineNumbers' => true,
                        'matchBrackets' => true,
                        'styleActiveLine' => true,
                        'theme' => 'ambiance',
                        'mode' => 'text/x-php',
                    ],
                ]);?>
                <?php // $form->field($model, 'input_data')->textarea(['rows' => 3, 'placeholder'=>'String array or function']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'input_validate')->widget(CodeMirror::classname(), [
                    'options' => [
                        'id' => 'input_validate',
                    ],
                    'clientOptions' => [
                        'lineNumbers' => true,
                        'matchBrackets' => true,
                        'styleActiveLine' => true,
                        'theme' => 'ambiance',
                        'mode' => 'text/x-php',
                    ],
                ]);?>
                <?php // $form->field($model, 'input_validate')->textarea(['rows' => 3]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'input_meta')->widget(CodeMirror::classname(), [
                    'options' => [
                        'id' => 'input_meta',
                    ],
                    'clientOptions' => [
                        'lineNumbers' => true,
                        'matchBrackets' => true,
                        'styleActiveLine' => true,
                        'theme' => 'ambiance',
                        'mode' => 'text/x-php',
                    ],
                ]);?>
                <?php // $form->field($model, 'input_meta')->textarea(['rows' => 10]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'input_specific')->widget(CodeMirror::classname(), [
                    'options' => [
                        'id' => 'input_specific',
                        'value' => $model->isNewRecord ? "['showLabels' => false,]" : $model['input_specific']
                    ],
                    'clientOptions' => [
                        'lineNumbers' => true,
                        'matchBrackets' => true,
                        'styleActiveLine' => true,
                        'theme' => 'ambiance',
                        'mode' => 'text/x-php',
                    ],
                ]);?>
                <?php // $form->field($model, 'input_specific')->textarea(['rows' => 3]) ?>
            </div>
            <div class="col-md-2 sdbox-col">
                <?php 
                    echo $form->field($model, 'input_order')->widget(TouchSpin::classname(), [
                        'options'=>['placeholder'=>'จัดเรียง...'],
                        'pluginOptions' => [
                            'buttonup_class' => 'btn btn-default', 
                            'buttondown_class' => 'btn btn-default', 
                            'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>', 
                            'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'
                        ]
                    ]);
                ?>
                <?php // $form->field($model, 'input_order')->textInput(['type'=>'number']) ?>
            </div>
            <div class="col-md-2 sdbox-col" style="padding-top: 25px;">
                <?= $form->field($model, 'status')->widget(CheckboxX::classname(), [
                    'pluginOptions'=>['threeState'=>false],
                    'labelSettings' => [
                        'label' => '',
                        'position' => CheckboxX::LABEL_RIGHT
                    ],
                    'options'=>['value' => $model->isNewRecord ? 1 : $model['status']]
                    
                ])->label('Show on Form');  ?>
            </div>
            <div class="col-md-2 sdbox-col" style="padding-top: 25px;">
                <?= !$model->isNewRecord ?  Html::a('<i class="glyphicon glyphicon-list"></i> Sort All',['/core/tables-fields/sort-input','id' => $model['table_id']],['class' => 'btn btn-primary','role' => 'modal-remote']) : ''; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'begin_html')->widget(CodeMirror::classname(), [
                    'options' => [
                        'id' => 'begin_html',
                        'value' => $model->isNewRecord ? 
                        '<div class="form-group">
    <label class="col-sm-2 control-label">ข้อความ</label>
        <div class="col-sm-4">' : $model['begin_html']
                    ],
                    'clientOptions' => [
                        'lineNumbers' => true,
                        'matchBrackets' => true,
                        'styleActiveLine' => true,
                        'theme' => 'ambiance',
                        'mode' => 'htmlmixed',
                    ],
                ]);?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'end_html')->widget(CodeMirror::classname(), [
                    'options' => [
                        'id' => 'end_html',
                        'value' => $model->isNewRecord ? 
                            '</div></div>' : $model['end_html']
                    ],
                    'clientOptions' => [
                        'lineNumbers' => true,
                        'matchBrackets' => true,
                        'styleActiveLine' => true,
                        'theme' => 'ambiance',
                        'mode' => 'htmlmixed',
                    ],
                ]);?>
            </div>
        </div>
        
        <div class="row">
                        <label for="">&nbsp;&nbsp; Code Preview</label>
            <div class="col-md-12">
                <pre>
                <div id="code-preview">
                    
                </div>
            </pre>
            </div>
        </div>

        <?= Html::activeHiddenInput($model, 'updated_at') ?>
        <?= Html::activeHiddenInput($model, 'updated_by') ?>
        <?= Html::activeHiddenInput($model, 'created_at') ?>
        <?= Html::activeHiddenInput($model, 'created_by') ?>


    </div>
    <div class="modal-footer">
    <?= Html::button('Get RawSql', ['class' => 'btn btn-success','id' => 'btn-generate-rawsql','disabled'=>(!isset($_GET['id']))]) ?>
        <?= Html::button('Generate Code', ['class' => 'btn btn-success','id' => 'btn-generate-code','disabled'=>(!isset($_GET['id']))]) ?>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-danger']) ?>
        <?php if (!Yii::$app->request->isAjax) : ?>
        <?= Html::a('Close',Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
        <?php else : ?>
        <?= Html::button('Close', ['class' => 'btn btn-default','data-dismiss' => 'modal']) ?>
        <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<div id="rules">
    
</div>

<?php if (Yii::$app->request->isAjax) : ?>
<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
		\$form.attr('action'), //serialize Yii2 form
		\$form.serialize()
    ).done(function(result){
		if(result.status == 'success'){
			". Noty::show('result.message', 'result.status') ."
			if(result.action == 'create'){
				$(\$form).trigger('reset');
				$.pjax.reload({container:'#tables-fields-grid-pjax'});
				//$('#rules').html(result.rules);
			} else if(result.action == 'update'){
				$(document).find('#modal-tables-fields').modal('hide');
				$.pjax.reload({container:'#tables-fields-grid-pjax'});
			}
		} else{
			". Noty::show('result.message', 'result.status') ."
		} 
    }).fail(function(){
		". Noty::show("'" . Html::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
    });
    return false;
});

");?>
<?php elseif (Yii::$app->controller->action->id == 'update') : ?>
<?php

$this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
		\$form.attr('action'), //serialize Yii2 form
		\$form.serialize()
    ).done(function(result){
		if(result.status == 'success'){
			". Noty::show('result.message', 'result.status') ."
			if(result.action == 'create'){
				$(\$form).trigger('reset');
				//$.pjax.reload({container:'#tables-fields-grid-pjax'});
				//$('#rules').html(result.rules);
			} else if(result.action == 'update'){
				$(document).find('#modal-tables-fields').modal('hide');
				//$.pjax.reload({container:'#tables-fields-grid-pjax'});
			}
		} else{
			". Noty::show('result.message', 'result.status') ."
		} 
    }).fail(function(){
		". Noty::show("'" . Html::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
    });
    return false;
});

");?>
<?php endif; ?>
<?php

$table_id = isset($model['table_id']) ? $model['table_id'] : 0;
$actionid = '"'.$model->action_id.'"';
$this->registerJs(<<<JS
/*
var ComponentsCodeEditors = function () {
    
    var mirror1 = function () {
        var myTextArea = document.getElementById('tablesfields-input_hint');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme:"ambiance",
            mode: 'text/x-php',
            //autofocus : true
        });
    }
		var mirror2 = function () {
        var myTextArea = document.getElementById('tablesfields-input_data');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme:"ambiance",
            mode: 'text/x-php',
            //autofocus : true
        });
    }
		var mirror3 = function () {
        var myTextArea = document.getElementById('tablesfields-input_validate');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme:"ambiance",
            mode: 'text/x-php',
            //autofocus : true
        });
    }
		var mirror4 = function () {
        var myTextArea = document.getElementById('tablesfields-input_meta');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme:"ambiance",
            mode: 'text/x-php',
            //autofocus : true
        });
    }
		var mirror5 = function () {
        var myTextArea = document.getElementById('tablesfields-input_specific');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme:"ambiance",
            mode: 'text/x-php',
            //autofocus : true
        });
    }

		var mirror6 = function () {
        var myTextArea = document.getElementById('tablesfields-begin_html');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme:"ambiance",
            mode: 'htmlmixed',
            //autofocus : true
        });
    }

		var mirror7 = function () {
        var myTextArea = document.getElementById('tablesfields-end_html');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme:"ambiance",
            mode: 'htmlmixed',
            //autofocus : true
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            mirror1();
						mirror2();
						mirror3();
						mirror4();
						mirror5();
						mirror6();
						mirror7();
        }
    };

}();
jQuery(document).ready(function() {    
   	ComponentsCodeEditors.init();
});
*/
$("#btn-generate-code").on("click", function (e) {
    e.preventDefault();
    $.ajax({
        "type":"GET",
        "url":"/core/tables-fields/gen-code",
        "data":{id:$table_id},
        "async": false,
        "dataType":"text",
        "success":function (result) {
                $('#code-preview').html(result);
        },
        "error":function (xhr, status, error) {
                swal(error, status, "error");
        }
    });
});
$("#btn-generate-rawsql").on("click", function (e) {
    e.preventDefault();
    $.ajax({
        "type":"POST",
        "url":"/core/tables-fields/get-rawsql",
        "data":{table_id:$table_id,actionid:$actionid},
        "async": false,
        "dataType":"json",
        "success":function (result) {
                $('#code-preview').text(result.join(''));
        },
        "error":function (xhr, status, error) {
                swal(error, status, "error");
        }
    });
});
JS
);?>
<?php
Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "", // always need it for jquery plugin
    "size" => "modal-lg"
])
?>
<?php Modal::end(); ?>