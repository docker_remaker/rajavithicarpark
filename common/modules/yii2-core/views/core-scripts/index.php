<?php

use msoft\helpers\Html;
use yii\helpers\Url;
use kartik\icons\Icon;
use yii\bootstrap\Modal;
use msoft\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel metronic\js\models\TbJsScriptsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
RegisterJS::regis(['sweetalert','ajaxcrud'],$this);

$this->title = 'Core Scripts';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= SwalAlert::widget(); ?>

<div class="tb-js-scripts-index">
    <div class="sdbox-header">
		<h3><?= $this->title ?></h3>
    </div>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?php Pjax::begin(["id" => "crud-datatable-pjax"]); ?>    
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'hover' => true,
        'condensed' => true,
        'pjax' => true,
        'panel' => [
        	'heading'=> false,
			'before'=>	
						Html::a(Html::getBtnAdd(),['/core/core-scripts/create'], ['class' => 'btn btn-success btn-sm','data-pjax' => 0]). ' ' .
					  	Html::button(Html::getBtnDelete(), ['data-url'=>Url::to(['tables-fields/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-tables-fields', 'disabled'=>true]),
			'after'=>false,
		],
        'export' => [
			'fontAwesome' => true
		],
        'columns' => [
            ['class' => 'msoft\widgets\grid\SerialColumn'],
            //'url',
            [
                'attribute'=>'url', 
                'value'=>function ($model, $key, $index, $widget) { 
                    return $model['url'];
                },
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>ArrayHelper::map(msoft\core\models\TbJsScripts::find()->orderBy('id')->asArray()->all(), 'url', 'url'), 
                'filterWidgetOptions'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
                'filterInputOptions'=>['placeholder'=>'Url'],
            ],
            //'code:ntext',
            [
                'header' => 'Class',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    return "<code>msoft\core\classes\JSQuery::generateJS('".$model['url']."',\$this);</code>";
                }
            ],
            [
                'attribute' => 'created_by',
                'value' => function ($model, $key, $index, $column) {
                    return isset($model->user->name) ? $model->user->name : '';
                }
            ],
            [
                'class' => 'msoft\widgets\ActionColumn',
                'template'=>'<div class="btn-group btn-group-sm" style="width:100px;"> {view} {update} {delete} </div>',
                'viewOptions' => [
					'class' => 'btn btn-sm btn-default',
					'data-action' => 'view'
                ],
                'updateOptions' => [
					'class' => 'btn btn-sm btn-default',
					'data-action' => 'update'
                ],
                'deleteOptions' => [
					'class' => 'btn btn-sm btn-default',
					'data-action' => 'delete'
				],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg"
])?>
<?php Modal::end(); ?>
