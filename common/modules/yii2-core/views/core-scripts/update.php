<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model metronic\js\models\TbJsScripts */

$this->title = 'Update Tb Js Scripts: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tb Js Scripts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tb-js-scripts-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
