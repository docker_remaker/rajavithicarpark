<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model metronic\core\models\CoreGenerate */

$this->title = 'Update Core Generate: ' . $model->gen_id;
$this->params['breadcrumbs'][] = ['label' => 'Core Generates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->gen_id, 'url' => ['view', 'id' => $model->gen_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="core-generate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
