<?php

namespace msoft\core\assets;

use yii\web\AssetBundle;

class CoreAsset extends AssetBundle {

	public $sourcePath = '@msoft/core/assets';
	public $css = [
		'css/style.css'
	];
	public $js = [
	];
	public $depends = [
		'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
		'msoft\core\assets\FontAwesomeAsset',
		'msoft\core\assets\bootbox\BootBoxAsset',
		'msoft\core\assets\notify\NotifyAsset',
	];

}
