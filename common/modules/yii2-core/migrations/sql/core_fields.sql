/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : db_nsdu

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-09-28 10:38:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for core_fields
-- ----------------------------
DROP TABLE IF EXISTS `core_fields`;
CREATE TABLE `core_fields` (
  `field_code` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT 'Code',
  `field_internal` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Internal',
  `field_class` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT 'Class',
  `field_name` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT 'Function',
  `field_meta` longtext CHARACTER SET utf8 NOT NULL COMMENT 'Option',
  `field_description` text CHARACTER SET utf8 NOT NULL COMMENT 'Description',
  PRIMARY KEY (`field_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of core_fields
-- ----------------------------
INSERT INTO `core_fields` VALUES ('activeCheckbox', '1', '\\yii\\helpers\\Html', 'activeCheckbox', '', '');
INSERT INTO `core_fields` VALUES ('activeDropDownList', '1', '\\yii\\helpers\\Html', 'activeDropDownList', '', '');
INSERT INTO `core_fields` VALUES ('ActiveHiddenInput', '0', '\\yii\\helpers\\Html', 'activeHiddenInput', '', '\'class\'=>\'form-control\'');
INSERT INTO `core_fields` VALUES ('Checkbox', '1', '', 'checkbox', '', '');
INSERT INTO `core_fields` VALUES ('CheckboxList', '1', '', 'checkboxList', '', 'option inline \r\n\'itemOptions\'=>[\'labelOptions\'=>[\'class\'=>\'checkbox-inline\']]');
INSERT INTO `core_fields` VALUES ('CheckboxX', '1', '\\msoft\\widgets\\CheckboxX::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('CKEditor', '1', '\\msoft\\widgets\\CKEditor::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('CodeMirror', '1', 'msoft\\widgets\\CodeMirror::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('ColorInput', '1', '\\msoft\\widgets\\ColorInput::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('DateControl', '1', '\\msoft\\widgets\\DateControl::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('DatePicker', '1', '\\msoft\\widgets\\DatePicker::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('DateRangePicker', '1', '\\msoft\\widgets\\DateRangePicker::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('DateTimePicker', '1', '\\msoft\\widgets\\DateTimePicker::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('DepDrop', '1', '\\msoft\\widgets\\DepDrop::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('DropDownList', '1', '', 'dropDownList', '', '\'prompt\'=>\'กรุณาเลือกเมลที่ต้องการ\'');
INSERT INTO `core_fields` VALUES ('Editable', '1', '\\msoft\\widgets\\Editable::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('HiddenInput', '1', 'HiddenInput', 'hiddenInput', '', '');
INSERT INTO `core_fields` VALUES ('Highcharts', '1', '\\msoft\\widgets\\Highcharts::classname()', 'Highcharts', '', '');
INSERT INTO `core_fields` VALUES ('ICheck', '1', '\\msoft\\widgets\\ICheck::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('MaskedInput', '1', '\\yii\\widgets\\MaskedInput::classname()', 'widget', '', '[\'mask\' => \'999-999-9999\']');
INSERT INTO `core_fields` VALUES ('RadioList', '1', '', 'radioList', '', 'option inline \r\n\'itemOptions\'=>[\'labelOptions\'=>[\'class\'=>\'radio-inline\']]');
INSERT INTO `core_fields` VALUES ('RangeInput', '1', '\\msoft\\widgets\\RangeInput::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('Select2', '1', '\\msoft\\widgets\\Select2::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('StarRating', '1', '\\msoft\\widgets\\StarRating::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('SwitchInput', '1', '\\msoft\\widgets\\SwitchInput::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('Textarea', '1', '', 'textarea', 'a:1:{s:4:\"rows\";i:3;}', '');
INSERT INTO `core_fields` VALUES ('TextInput', '1', '', 'textInput', 'a:1:{s:9:\"maxlength\";b:1;}', '\'placeholder\'=>\'description\'\n\'readonly\'=>true\n\'disabled\'=>true\n\'type\'=>\'number\'\n\'step\'=>0.1\n\'min\'=>0\n\'max\'=>10');
INSERT INTO `core_fields` VALUES ('TimePicker', '1', '\\msoft\\widgets\\TimePicker::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('TinyMce', '1', '\\msoft\\widgets\\TinyMce::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('TouchSpin', '1', '\\msoft\\widgets\\TouchSpin::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('Typeahead', '1', '\\msoft\\widgets\\Typeahead::classname()', 'widget', '', '');
INSERT INTO `core_fields` VALUES ('Upload', '1', '\\trntv\\filekit\\widget\\Upload::classname()', 'widget', '', '');
