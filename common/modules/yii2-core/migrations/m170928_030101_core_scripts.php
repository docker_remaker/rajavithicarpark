<?php

use yii\db\Schema;

class m170928_030101_core_scripts extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('core_scripts', [
            'id' => $this->primaryKey(),
            'url' => $this->string(255)->notNull(),
            'code' => $this->text()->notNull(),
            'created_by' => $this->integer(11),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);
                
    }

    public function down()
    {
        $this->dropTable('core_scripts');
    }
}
