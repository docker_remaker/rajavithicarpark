<?php

use yii\db\Schema;

class m170928_030101_core_fields extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('core_fields', [
            'field_code' => $this->string(20)->notNull(),
            'field_internal' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'field_class' => $this->string(80)->notNull(),
            'field_name' => $this->string(30)->notNull(),
            'field_meta' => $this->text()->notNull(),
            'field_description' => $this->text()->notNull(),
            'PRIMARY KEY ([[field_code]])',
            ], $tableOptions);
                
    }

    public function down()
    {
        $this->dropTable('core_fields');
    }
}
