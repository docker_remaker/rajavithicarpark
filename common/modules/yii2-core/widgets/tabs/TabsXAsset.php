<?php

namespace msoft\widgets\tabs;

use msoft\widgets\base\AssetBundle;

class TabsXAsset extends AssetBundle
{
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets/bootstrap-tabs-x');
        $this->setupAssets('css', ['css/bootstrap-tabs-x']);
        $this->setupAssets('js', ['js/bootstrap-tabs-x']);
        parent::init();
    }
}
