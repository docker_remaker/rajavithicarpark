<?php

namespace msoft\widgets\select2;

use msoft\widgets\base\AssetBundle;

class ThemeKrajeeAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/select2-krajee']);
        parent::init();
    }
}
