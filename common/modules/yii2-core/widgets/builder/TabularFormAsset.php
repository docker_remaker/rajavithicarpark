<?php

namespace msoft\widgets\builder;

use msoft\widgets\base\AssetBundle;

class TabularFormAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/tabular-form']);
        parent::init();
    }
}
