<?php
namespace msoft\widgets\field;

class FieldRangeAsset extends \msoft\widgets\base\AssetBundle
{
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/field-range']);
        $this->setupAssets('js', ['js/field-range']);
        parent::init();
    }

}