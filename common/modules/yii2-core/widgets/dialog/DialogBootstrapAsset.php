<?php

namespace msoft\widgets\dialog;
use msoft\widgets\base\PluginAssetBundle;

class DialogBootstrapAsset extends PluginAssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets/bootstrap3-dialog');
        $this->setupAssets('js', ['dist/js/bootstrap-dialog']);
        $this->setupAssets('css', ['dist/css/bootstrap-dialog']);
        parent::init();
    }
}
