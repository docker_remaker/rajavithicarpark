<?php

/**
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2015 - 2017
 * @package yii2-date-range
 * @version 1.6.8
 */

namespace msoft\widgets\daterange;

use msoft\widgets\base\AssetBundle;

class DateRangePickerAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $depends = [
        '\msoft\widgets\daterange\MomentAsset',
        'yii\web\JqueryAsset'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/daterangepicker', 'css/daterangepicker-kv']);
        $this->setupAssets('js', ['js/daterangepicker']);
        parent::init();
    }
}
