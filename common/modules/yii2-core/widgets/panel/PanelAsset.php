<?php
namespace msoft\widgets\panel;

use yii\web\AssetBundle;


class PanelAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';
    public $css = [
//        'panel-widget.css',
    ];
    public $js = [
        'js/jquery.panel-widget.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}