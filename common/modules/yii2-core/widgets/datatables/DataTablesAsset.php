<?php

namespace msoft\widgets\datatables;


use yii\web\AssetBundle;

class DataTablesAsset extends AssetBundle
{
    const STYLING_DEFAULT = 'default';
    const STYLING_BOOTSTRAP = 'bootstrap';
    const STYLING_MATERIAL = 'material';
    const STYLING_JUI = 'jqueryui';

    public $styling = self::STYLING_BOOTSTRAP;
    public $sourcePath = __DIR__ . '/assets';

    public $cssOptions = ['position' => \yii\web\View::POS_HEAD];
    public $jsOptions = ['position' => \yii\web\View::POS_END];

    public $depends = [
        'yii\web\JqueryAsset',
        'msoft\widgets\grid\GridViewAsset',
    ];

    public function init()
    {
        parent::init();

        switch ($this->styling) {
            case self::STYLING_JUI:
                $this->depends[] = 'yii\jui\JuiAsset';
                $this->css[] = 'media/css/dataTables.jqueryui.min.css';
                $this->css[] = 'extensions/Responsive/css/responsive.bootstrap.min.css';
                $this->js[] = 'media/js/jquery.dataTables.min.js';
                $this->js[] = 'media/js/dataTables.jqueryui.min.js';
                $this->js[] = 'extensions/Responsive/js/dataTables.responsive.min.js';
                break;
            case self::STYLING_BOOTSTRAP:
                $this->depends[] = 'yii\bootstrap\BootstrapAsset';
                $this->depends[] = 'msoft\core\assets\FontAwesomeAsset';
                $this->css[] = 'datatables.min.css';
                //$this->css[] = 'plugins/bootstrap/datatables.bootstrap.css';
                $this->css[] = 'Responsive/css/responsive.bootstrap.min.css';
                $this->js[] = 'datatable.js';
                $this->js[] = 'datatables.min.js';
                $this->js[] = 'plugins/bootstrap/datatables.bootstrap.js';
                $this->js[] = 'Responsive/js/responsive.bootstrap.min.js';
                break;
            case self::STYLING_MATERIAL:
                $this->depends[] = 'yii\bootstrap\BootstrapAsset';
                $this->depends[] = 'msoft\core\assets\FontAwesomeAsset';
                $this->css[] = 'media/css/material.min.css';
                $this->css[] = 'extensions/Responsive/css/responsive.bootstrap.min.css';
                $this->css[] = 'media/css/dataTables.material.min.css';
                $this->js[] = 'media/js/jquery.dataTables.min.js';
                $this->js[] = 'media/js/dataTables.material.min.js';
                $this->js[] = 'extensions/Responsive/js/dataTables.responsive.min.js';
                break;
            case self::STYLING_DEFAULT:
                $this->css[] = 'media/css/jquery.dataTables.min' . (YII_ENV_DEV ? '' : '.min') . '.css';
                $this->css[] = 'extensions/Responsive/css/responsive.bootstrap.min.css';
                $this->js[] = 'media/js/jquery.dataTables.min.js';
                $this->js[] = 'extensions/Responsive/js/dataTables.responsive.min.js';
                break;
            default;
        }
    }

} 