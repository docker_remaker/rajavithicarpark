<?php


namespace msoft\widgets\knob;


use yii\web\AssetBundle;

class KnobAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/assets';
    public $js = ['js/jquery.knob.min.js'];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
} 