<?php

namespace msoft\widgets\detailview;

use msoft\widgets\base\AssetBundle;

class DetailViewAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/kv-detail-view']);
        $this->setupAssets('css', ['css/kv-detail-view']);
        parent::init();
    }
}
