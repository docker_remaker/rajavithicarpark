<?php

namespace msoft\widgets\croppie;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;

class Croppie extends Widget
{
    /**
     * @var array the displayed tag option
     */
    public $options = [];
    /**
     * @var array the plugin options
     * @see http://foliotek.github.io/Croppie/
     */
    public $clientOptions = [];
    /**
     * @var array the plugin events.
     * @see http://foliotek.github.io/Croppie/
     */
    public $clientEvents = [];

    public $bind = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo Html::tag('div', '', $this->options);

        $this->registerClientScript();
    }

    /**
     * Registers required script for the plugin to work
     */
    public function registerClientScript()
    {
        $view = $this->getView();

        CroppieAsset::register($view);

        if (isset($this->clientOptions['enableExif']) && $this->clientOptions['enableExif'] === true) {
            ExifJsAsset::register($view);
        }

        $options = !empty($this->clientOptions) ? Json::encode($this->clientOptions) : '';
        $bind = !empty($this->bind) ? Json::encode($this->bind) : '';

        $id = $this->getId();

        $js[] = "var croppie".str_replace('-','',$id)." = $('#$id').croppie($options);";
        if(isset($this->bind)){
            $js[] = "croppie".str_replace('-','',$id).".croppie('bind',$bind);";
        }
        if (!empty($this->clientEvents)) {
            foreach ($this->clientEvents as $event => $handler) {
                $js[] = "jQuery('#$id').on('$event', $handler);";
            }
        }

        $view->registerJs(implode("\n", $js));
    }
}
