<?php
namespace msoft\widgets\toastr;

use yii\helpers\Json;

/**
 * Class NotificationFlash
 * @package msoft\widgets\toastr
 */
class ToastrFlash extends Toastr
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $session = \Yii::$app->session;

        $flashes = $session->getAllFlashes();

        foreach ($flashes as $type => $data) {

            $data = (array) $data;

            foreach ($data as $i => $message) {

                Toastr::widget(['type' => $type, 'message' => $message, 'options' => ($this->options) ? Json::decode($this->options) : []]);

            }

            $session->removeFlash($type);
        }
    }
}
