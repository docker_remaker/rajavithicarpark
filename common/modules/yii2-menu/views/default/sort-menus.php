<?php 
use msoft\menu\assets\SortMenuAsset;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use msoft\widgets\Icon;
use yii\helpers\Html;
Icon::map($this);
SortMenuAsset::register($this);

$this->title = 'จัดเรียงเมนู';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
.h-bg-navy-blue {
    background: #34495e;
}
</style>
<div class="row">
    <div class="col-sm-2">
        <?php echo $this->render('../layouts/tabs-menu.php',['active' => 'จัดเรียงเมนู']); ?>
    </div>
    <div class="col-sm-10">
        <div class="tab-content printable">
            <div id="tabs1" class="tab-pane fade active in">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div id="nestable-menu">
                            <button type="button" data-action="expand-all" class="btn btn-default btn-sm">Expand All</button>
                            <button type="button" data-action="collapse-all" class="btn btn-default btn-sm">Collapse All</button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?= Yii::$app->id; ?></h3>
                            </div>
                            <div class="panel-body">
                                <div class="dd" id="nestable2">
                                    <ol class="dd-list">
                                        <?php foreach ($rows as $item): ?>
                                            <?php if($item['parent_id'] == null): ?>
                                            <li class="dd-item" data-id="<?php echo $item["id"]; ?>">
                                                <div class="dd-handle">
                                                    <span class="pull-right"> <?php echo $item["router"]; ?> </span>
                                                    <?php echo !empty($item["icon"]) ? '<span class="label h-bg-navy-blue">'.Icon::show($item["icon"]).'</span>' : ''; ?>
                                                    <?php echo strip_tags($item["title"]); ?>
                                                </div>     
                                                <?php if ($item['parent_id'] == null): ?>
                                                    <?php $parents = ArrayHelper::index($rows, null, 'parent_id'); ?>
                                                    <?php if(isset($parents[$item["id"]])): ?>
                                                        <ol class="dd-list">
                                                            <?php foreach ($parents[$item["id"]] as $parent): ?>
                                                                <li class="dd-item" data-id="<?php echo $parent["id"]; ?>">
                                                                    <div class="dd-handle">
                                                                        <span class="pull-right"> <?php echo $parent["router"]; ?> </span>
                                                                        
                                                                        <?php echo !empty($parent["icon"]) ? '<span class="label h-bg-navy-blue">'.Icon::show($parent["icon"]).'</span>' : ''; ?>
                                                                        
                                                                        <?php echo strip_tags($parent["title"]); ?>
                                                                    </div>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ol>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
$this->registerJs(<<<JS
$('.dd').nestable('collapseAll');

$('#nestable-menu').on('click', function (e) {
    var target = $(e.target),
            action = target.data('action');
    if (action === 'expand-all') {
        $('.dd').nestable('expandAll');
    }
    if (action === 'collapse-all') {
        $('.dd').nestable('collapseAll');
    }
});

$('.dd').on('change', function() {
    $.ajax({
        method: "POST",
        url: "savemenu-onchange",
        data: {
            items: $('.dd').nestable('serialize')
        },
        success: function (data) {
            toastr.success('The changes have been Saved!');
        },
    }).fail(function (jqXHR, textStatus, errorThrown) {
        console.log(errorThrown);
    });
});

setTimeout(function(){ $('.dd').nestable('collapseAll'); }, 100);
JS
,yii\web\View::POS_END);?>