// Import modules serialport
var SerialPort = require('serialport');
//var request = require('request');
var Readline = SerialPort.parsers.Readline;

//import modules socket io
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

//SQL
// var mysql = require('mysql');
// var connection = mysql.createConnection({
//   host     : 'localhost',
//   user     : 'root',
//   password : '',
//   database : 'db_nsdu'
// });

// connection.connect();

// function doCall(urlToCall, callback) {
//     request(urlToCall, function (err, data, response) {
//         return callback(response);
//     });
// }

// var urlToCall = "http://localhost:3000/com-setting/2";
// doCall(urlToCall, function(response){
//  return response;
// });







io.on('connection', function(socket){
  // console.log('a user connected');
  // socket.on('disconnect', function(){
  //   console.log('user disconnected');
  // });
});

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

//====================== ===========================//
var portNameG1 = '/dev/ttyS0';
var portNameG2 = '/dev/ttyS1';
var portNameG3 = '/dev/ttyS2';
var clientPort = 3000;
var Hex,CardID;
var portG1 = new SerialPort(portNameG1, {});
var portG2 = new SerialPort(portNameG2, {});
var portG3 = new SerialPort(portNameG3, {});
var parserG1 = portG1.pipe(new Readline({ delimiter: '\r\n' }));
var parserG2 = portG2.pipe(new Readline({ delimiter: '\r\n' }));
var parserG3 = portG3.pipe(new Readline({ delimiter: '\r\n' }));

//list serial ports:
SerialPort.list(function (err, ports) {
 console.log(ports);
     ports.forEach(function(port) {
     console.log(port.comName);
     console.log(port.pnpId);
     console.log(port.manufacturer);
 });
});

portG1.on('error', function(err) {
  console.log('Error: ', err.message);
});
portG2.on('error', function(err) {
  console.log('Error: ', err.message);
});
portG3.on('error', function(err) {
  console.log('Error: ', err.message);
});
http.listen(clientPort, function(){
  console.log('listening on *:'+clientPort);
});

parserG1.on('data', function (data) {
    Hex = hexData(data);
    CardID = parseInt(Hex, 16);//Ex. 1355300
    console.log('Read SerialPort : '+portNameG1);
    console.log(CardID);
    // request('http://localhost:3001/api/' + CardID, function (error, response, body) {
    //   console.log('error:', error); // Print the error if one occurred
    //   console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
    //   console.log('body:', body); // Print the HTML for the Google homepage.
    // });
    io.emit('request_G1', {
        source: CardID,
        port : portNameG1,
    });
});
parserG2.on('data', function (data) {
    Hex = hexData(data);
    CardID = parseInt(Hex, 16);//Ex. 1355300
    console.log('Read SerialPort : '+portNameG2);
    console.log(CardID);
    io.emit('request_G2', {
        source: CardID,
        port : portNameG2,
    });
});
parserG3.on('data', function (data) {
    Hex = hexData(data);
    CardID = parseInt(Hex, 16);//Ex. 1355300
    console.log('Read SerialPort : '+portNameG3);
    console.log(CardID);
    io.emit('request_G3', {
        source: CardID,
        port : portNameG3,
    });
});
function hexData(data){
    var HexStr = data.replace(/^\D+/g, '');//Ex. 040014AE24
    return HexStr.substring(4, 11);//Ex. 14AE24
}


