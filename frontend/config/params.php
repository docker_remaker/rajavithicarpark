<?php
return [
    'adminEmail' => 'admin@example.com',
    'Copyright' => 'Copyright © 2017 Andaman PT. All Rights Reserved',
    'webTitle' => 'ระบบจัดการลานจอดรถยนต์',
    'appName' => 'ระบบจัดการลานจอดรถยนต์ อาคารเฉลิมพระเกียรติ โรงพยาบาลราชวิถี'
];
