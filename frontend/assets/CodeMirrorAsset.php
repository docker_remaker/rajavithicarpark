<?php
namespace metronic\assets;

use Yii;
use yii\web\AssetBundle;

class CodeMirrorAsset extends AssetBundle
{
    public $sourcePath = '@metronic/assets';
    public $css = [
        'global/plugins/codemirror/lib/codemirror.css?v=1.0',
        'global/plugins/codemirror/theme/ambiance.css?v=1.0'
    ];

    public $js = [
        'global/plugins/codemirror/lib/codemirror.js?v=1.0',
        'global/plugins/codemirror/mode/javascript/javascript.js?v=1.0',
        'global/plugins/codemirror/mode/htmlmixed/htmlmixed.js?v=1.0',
        'global/plugins/codemirror/mode/css/css.js?v=1.0',
        'global/plugins/codemirror/mode/xml/xml.js?v=1.0',
        'global/plugins/codemirror/mode/php/php.js?v=1.0',
        'global/plugins/codemirror/mode/clike/clike.js?v=1.0',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}