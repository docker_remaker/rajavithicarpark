<?php
namespace metronic\assets;

use Yii;
use yii\web\AssetBundle;

class WaitMeAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/global/plugins/waitMe';
    public $css = [
        'waitMe.min.css?v=1.0'
    ];

    public $js = [
        'waitMe.min.js?v=1.0'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}