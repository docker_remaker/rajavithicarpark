<?php

namespace frontend\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\setting\models\TbConfigCamera;

/**
 * TbConfigCameraSearch represents the model behind the search form about `frontend\modules\setting\models\TbConfigCamera`.
 */
class TbConfigCameraSearch extends TbConfigCamera
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gate_id'], 'integer'],
            [['camera_name', 'ip', 'port', 'username', 'password'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbConfigCamera::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gate_id' => $this->gate_id,
        ]);

        $query->andFilterWhere(['like', 'camera_name', $this->camera_name])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'port', $this->port])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password]);

        return $dataProvider;
    }
}
