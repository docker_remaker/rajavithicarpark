<?php

namespace frontend\modules\setting\models;

use Yii;

/**
 * This is the model class for table "tb_config_report".
 *
 * @property integer $id
 * @property integer $gate13_timediff_hr
 * @property integer $gate12_timediff_min
 * @property string $parking_name
 * @property string $company_name
 * @property string $rec_comment
 * @property integer $parkinglotqty_visitor
 * @property integer $parkinglotqty_member
 */
class TbConfigReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_config_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'gate13_timediff_hr', 'gate12_timediff_min', 'parkinglotqty_visitor', 'parkinglotqty_member'], 'integer'],
            [['parking_name', 'company_name'], 'string', 'max' => 100],
            [['rec_comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gate13_timediff_hr' => 'เกณฑ์เวลา ค้างคืน(ชั่วโมง)',
            'gate12_timediff_min' => 'เกณฑ์เวลา จอดไม่ตรงตามพื้นที่(นาที)',
            'parking_name' => 'ชื่อลานจอด',
            'company_name' => 'ชื่อหน่วยงาน',
            'rec_comment' => 'หมายเหตุ-ข้อความ',
            'parkinglotqty_visitor' => 'จำนวนที่จอดรถผู้รับบริการ',
            'parkinglotqty_member' => 'จำนวนที่จอดรถเจ้าหน้าที่',
        ];
    }
}
