<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\setting\models\TbConfigRelay */

$this->title = 'Create Tb Config Relay';
$this->params['breadcrumbs'][] = ['label' => 'Tb Config Relays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-config-relay-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
