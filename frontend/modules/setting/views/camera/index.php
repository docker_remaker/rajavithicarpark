<?php

use yii\helpers\Html;
use metronic\widgets\portlet\PortletBox;
use msoft\widgets\Icon;
use metronic\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use msoft\widgets\Datatables;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\setting\models\TbConfigCameraSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ตั้งค่ากล้อง';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= PortletBox::begin([
    'title'=> Html::tag('span',$this->title,['class' => 'caption-subject bold uppercase']),
    'icon' => Icon::show('icon-settings font-dark',[],Icon::I),
    'captionHelper' => '',
    'options' => [
        'class' => 'portlet light bordered'
    ],
    'tools' => [],
    'actions' => [
        //Html::a(Icon::show('plus').' Add', ['create'], ['class' => 'btn dark btn-outline','style' => 'margin-right:440px;','role'=>'modal-remote'])
    ]
]);
?>
<div class="tb-config-camera-index">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= Datatables::widget([
        'dataProvider' => $dataProvider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'export' => false,
        'hover' => true,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'msoft\widgets\grid\SerialColumn'],
            [
                'attribute' => 'gate_id',
                'label' => 'ชื่อประตู',
                'vAlign' => 'middle',
                'hAlign' => 'left',
                'value' => function($model) {
                    return @$model->gate->gate_name;
                }
            ],
            'camera_name',
            'ip',
            'port',
            'username',
            'password',
            [
                'class' => 'msoft\widgets\ActionColumn',
                'noWrap' => TRUE,
                'buttons'=> [
                'view' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, 
                    [
                        'class' => 'btn btn-xs btn-success tooltips',
                        'role' => 'modal-remote',
                        'data-pjax' => false,
                        'title' => Yii::t('yii', 'View'),
                    ]);
                },
                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, 
                    [
                        'class' => 'btn btn-xs btn-primary tooltips',
                        'data-pjax' => false,
                        'title' => Yii::t('yii', 'Update'),
                    ]);
                },
                'delete' => function ($url, $model, $key) {
                    return '';
                    // return Html::a('<span class="glyphicon glyphicon-trash"></span>',$url, 
                    // [
                    //     'class' => 'btn btn-xs btn-danger tooltips',
                    //     'data-pjax' => false,
                    //     'title' => Yii::t('yii', 'Delete'),
                    //     'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    //     'data-method' => 'post',

                    // ]);
                },
            ],
        ], 
        ],
    ]); ?>
</div>
<?= PortletBox::end(); ?>

<?php echo $this->render('modal'); ?>