<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\setting\models\TbConfigCamera */

$this->title = 'ตั้งค่ากล้อง';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'บันทึกตั้งค่า';
?>
<div class="tb-config-camera-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
