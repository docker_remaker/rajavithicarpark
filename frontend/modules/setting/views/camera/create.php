<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\setting\models\TbConfigCamera */

$this->title = 'Create Tb Config Camera';
$this->params['breadcrumbs'][] = ['label' => 'Tb Config Cameras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-config-camera-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
