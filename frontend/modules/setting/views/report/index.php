<?php

use yii\helpers\Html;
use msoft\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use metronic\helpers\RegisterJS;
RegisterJS::regis(['ajaxcrud','waitme'], $this);

$this->title = 'ตั้งค่าหน้ารายงาน';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss("
div.col-xs-11{
    top: 60px !important;
}
");
?>
<?php
echo Tabs::widget([
    'items' => [
        [
            'label' => $this->title,
            'content' => 'Anim pariatur cliche...',
            'active' => true,
        ],
    ],
    'encodeLabels' => false,
    'renderTabContent' => false
]);
?>
<div class="tab-content">
    <div class="tb-setting-form">
        <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL,'id'=>'form_setting']); ?>
        <div class="form-group">
          <label class="col-sm-2 control-label">ชื่อลานจอด</label>
            <div class="col-sm-8">
            <?= $form->field($model, 'parking_name', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                ]);
            ?>
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">ชื่อหน่วยงาน</label>
            <div class="col-sm-8">
            <?= $form->field($model, 'company_name', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                ]);
            ?>
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">หมายเหตุ-ข้อความ</label>
            <div class="col-sm-8">
            <?= $form->field($model, 'rec_comment', 
                ['showLabels' => false])->textArea([
                    'maxlength' => true,
                    'rows' => 3,
                ]);
            ?>
            </div>
        </div> 
        <div class="form-group">
          <label class="col-sm-2 control-label">ค้างคืน(ชั่วโมง)</label>
            <div class="col-sm-8">
            <?= $form->field($model, 'gate13_timediff_hr', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                ]);
            ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">จอดไม่ตรงตามพื้นที่(นาที)</label>
            <div class="col-sm-8">
            <?= $form->field($model, 'gate12_timediff_min', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                ]);
            ?>
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">จำนวนที่จอดรถผู้รับบริการ</label>
            <div class="col-sm-8">
            <?= $form->field($model, 'parkinglotqty_visitor', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                ]);
            ?>
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">จำนวนที่จอดรถเจ้าหน้าที่</label>
            <div class="col-sm-8">
            <?= $form->field($model, 'parkinglotqty_member', 
                ['showLabels' => false])->textInput([
                    'maxlength' => true,
                ]);
            ?>
            </div>
        </div>

        <br>        
        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group" style="text-align:right;">
                <div class="col-sm-10">
                    <?= Html::a('Close', ['index'], ['class' => 'btn btn-default']) ?>
                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Save', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
                </div>
            </div>
        <?php } ?>
        <?php ActiveForm::end(); ?>

    </div>
</div>
<?php
$this->registerJs(<<<JS
$( document ).ready(function() {
    $('#form_setting').on('beforeSubmit', function(e){
        LoadingClass();
    });
});
JS
);?>
<script type="text/javascript">
function LoadingClass() {
    $('.page-content').waitMe({
        effect : 'orbit',
        text: 'กำลังโหลดข้อมูล...',
        bg : 'rgba(255,255,255,0.7)',
        color : '#000',
        maxSize : '',
        textPos : 'vertical',
        fontSize : '18px',
        source : ''
    });
}
</script>