<?php

namespace frontend\modules\inventory\components;

use Yii;
use msoft\widgets\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use frontend\components\GridBuilder;
use msoft\widgets\Icon;
use frontend\modules\inventory\components\InventoryQuery;

class FormColumns extends GridBuilder
{
    public function getGridReceiveOption($model,$btn=null)
    {
        return [
            'panel' => [
                'heading' => false,
                'before' => $btn,
                'after' => false,
            ],
           'toolbar' => [
                [
                    'content' => false,
                ],
            ],
            'tableOptions' => [
                'id' => 'tb-tabular',
            ],
            'hover' => true,
            'bordered' => true,
            'condensed' => true,
            'striped' => true,
            'layout' => "{items}",
            'responsive' => false,
        ];
    }
}