<?php

namespace frontend\modules\payment\models;

use Yii;

/**
 * This is the model class for table "tb_receipt".
 *
 * @property int $rec_num เลขที่ใบเสร็จรับเงิน
 * @property int $trans_id เลขที่รายการ
 * @property string $fee_amt ค่าบริการจอดรถยนต์
 * @property string $disc_amt ส่วนลด
 * @property string $cardloss_amt ค่าปรับบัตรหาย
 * @property string $total_paid รวมเป็นเงิน
 * @property int $inv_num เลขที่การนำสั่งเงินสด
 * @property int $print_status สถานะการสั่งพิมพ์
 */
class TbReceipt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_receipt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trans_id', 'inv_num', 'print_status'], 'integer'],
            [['fee_amt', 'disc_amt', 'cardloss_amt', 'total_paid'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rec_num' => 'เลขที่ใบเสร็จรับเงิน',
            'trans_id' => 'เลขที่รายการ',
            'fee_amt' => 'ค่าบริการจอดรถยนต์',
            'disc_amt' => 'ส่วนลด',
            'cardloss_amt' => 'ค่าปรับบัตรหาย',
            'total_paid' => 'รวมเป็นเงิน',
            'inv_num' => 'เลขที่การนำสั่งเงินสด',
            'print_status' => 'สถานะการสั่งพิมพ์',
        ];
    }
}
