<?php

namespace frontend\modules\payment\models;

use Yii;

/**
 * This is the model class for table "tb_shift".
 *
 * @property int $shift_id
 * @property string $shiff_begin
 * @property string $shiff_end
 */
class TbShift extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_shift';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shiff_begin', 'shiff_end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'shift_id' => 'Shift ID',
            'shiff_begin' => 'Shiff Begin',
            'shiff_end' => 'Shiff End',
        ];
    }
}
