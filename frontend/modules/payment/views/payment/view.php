<?php

use yii\helpers\Html;
use msoft\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payment\models\TbReceipt */

$this->title = $model->rec_num;
$this->params['breadcrumbs'][] = ['label' => 'Tb Receipts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-receipt-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'rec_num',
            'trans_id',
            'fee_amt',
            'disc_amt',
            'cardloss_amt',
            'total_paid',
            'inv_num',
            'print_status',
        ],
    ]) ?>

</div>
