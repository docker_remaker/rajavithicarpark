$('body').addClass('page-sidebar-closed');
$('.page-sidebar .page-sidebar-menu, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu').addClass('page-sidebar-menu-closed');
//Form
Project = {
    Test : function(shift_id){
        if(shift_id != ''){
            $('input[name="shift_begin"]').val(shift[shift_id][0].shiff_begin);
            $('input[name="shift_end"]').val(shift[shift_id][0].shiff_end);
        }else{

            $('input[name="shift_begin"]').val(null);
            $('input[name="shift_end"]').val(null);
        }
    },
    AutoSave: function(){
        var frm = $('form[id="TbProject"]');
        //var Form = $('.tb-meeting-form').find("form")[0];
        var data = new FormData($(frm)[0]);
        console.log(data);
        $.ajax({
            "type":frm.attr('method'),
            "url": frm.attr('action'),//frm.attr('action')
            "data":data,
            "async": false,
            "dataType":"json",
            "success":function (result) {
                AppNotify.SaveCompleted();
            },
            "error":function (xhr, status, error) {
                AppNotify.Error(error);
            },
            contentType: false,
            cache: false,
            processData: false
        });
        return false;
    },
    initTabsActive: function(){
        //sessionStorage.removeItem('activeid');
        var sessionID = sessionStorage.getItem("activeid");
        if(sessionID !== null && (window.location.pathname === '/project/prj/update' || window.location.pathname === '/project/prj/sumup-project')){
            //Remove Class
            $('ul#tabs-left > li.active').removeClass('active');
            for (i = 0; i < 15; i++) { 
                $('div[id="tabs'+i+'"]').removeClass('active in');
            }
            //Add Class
            var tabsid = sessionID.replace("#", "");
            $('li[data-id="'+tabsid+'"]').addClass('active');
            $('div[id="'+tabsid+'"]').addClass('active in');
        }
    }
};

var $form = $('#form_report');
$form.on('beforeSubmit', function() {
    var data = $form.serialize();
    var table = $('#example1').DataTable();
    
    var dataArray = $($form).serializeArray(),
                dataObj = {};
        $(dataArray).each(function (i, field) {
            dataObj[field.name] = field.value;
        });
    var keys = [];
    //$('#payment_datatable').yiiGridView('getSelectedRows');
    $('input[name="selection[]"]').each(function() {
        if($(this).is(':checked')) {
            keys.push($(this).val());
        }
    });
    if(keys.length == 0){
        Notify.warning('กรุณาเลือกรายการ');
        localStorage.clear();
    }else if(dataObj['shift_id'] == ''){
        Notify.warning('กรุณาเลือกช่วงเวลา (กะ)');
    }else{
        swal({
            title: "บันทึกนำส่งเงินสด?",
                text: "ยืนยันการบันทึกข้อมูลนำส่งเงินสด",
                icon: "warning",
                buttons: true,
            }).then((willDelete) => {
                Loading.hide();
                if (willDelete) {
                    Loading.class();
                    $.ajax({
                        url: "savepayment",
                        data : {shift_id:dataObj['shift_id'],keys:keys},
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {
                            if(data != false){
                                table.ajax.reload();
                                //$.pjax.reload({container: '#index'});
                                Notify.success('บันทึกข้อมูลเรียบร้อย');
                                Loading.hide();
                                $('#ajaxCrudModal').modal('hide');
                                window.open('findidprint','_blank');
                            }else{
                                Notify.error('error');
                            }
                            
                        },
                        error: function (xhr, status, error) {
                            Notify.error('error');
                            localStorage.clear();
                            Loading.hide();
                        }
                    });
                } 
            });
    }
    return false; // prevent default submit
});
//
/*
$(function(){
    $('#TbProject input,select,textarea').on('change',function(e){
        Project.AutoSave();
    });
});*/
// $(function(){
//     //Loading
//     $('.page-container').waitMe({
//         effect : 'facebook',
//         text : 'Please wait...',
//         bg : 'rgba(255,255,255,0.7)',
//         color : '#000',
//         textPos : 'vertical',
//     });

//     $('ul#tabs-left > li >a').on('click',function(e){
//         if(window.location.pathname === '/project/prj/update' || window.location.pathname === '/project/prj/sumup-project'){
//             sessionStorage.setItem('activeid',$(this).attr('href'));
//         }
//     });
//     Project.initTabsActive();
// });

// window.onload = function () { $(".page-container").waitMe("hide"); }