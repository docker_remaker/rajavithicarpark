<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payment\models\TbReceipt */

$this->title = 'Update Tb Receipt: ' . $model->rec_num;
$this->params['breadcrumbs'][] = ['label' => 'Tb Receipts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->rec_num, 'url' => ['view', 'id' => $model->rec_num]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tb-receipt-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
