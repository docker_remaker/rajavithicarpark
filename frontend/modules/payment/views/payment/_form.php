<?php

use yii\helpers\Html;
use msoft\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payment\models\TbReceipt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-receipt-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);?>

    <?= $form->field($model, 'trans_id')->textInput() ?>

    <?= $form->field($model, 'fee_amt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'disc_amt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cardloss_amt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_paid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'inv_num')->textInput() ?>

    <?= $form->field($model, 'print_status')->textInput() ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group" style="text-align:right;">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>
