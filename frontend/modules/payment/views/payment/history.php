<?php

use msoft\helpers\Html;
use msoft\widgets\Panel;
use msoft\widgets\Icon;
use msoft\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use msoft\widgets\Datatables;
use metronic\widgets\portlet\PortletBox;
use yii\data\ArrayDataProvider;
use yii\grid\GridViewAsset;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payment\models\TbReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
RegisterJS::regis(['sweetalert'],$this);
GridViewAsset::register($this);

$this->title = 'ประวัตินำส่งเงินสด';
$this->params['breadcrumbs'][] = $this->title;
$classmidle = ['class' => 'kv-align-center kv-align-middle','style' => 'color:black'];
$this->registerCss('
    .btn-default {
        background : #fff !important;
    }
');
$provider = new ArrayDataProvider([
    'allModels' => [],
    'pagination' => [
        'pageSize' => 100,
    ],
]);
?>
<?= SwalAlert::widget(); ?>
<div class="tab-content">
<?php echo $this->render('_tab'); 
$this->registerJs('$("#tab_B").addClass("active");');
?>
<?= PortletBox::begin([
    'title' => '<span class="caption-subject bold uppercase">' . $this->title . '</span>',
    'icon' => 'fa fa-cube font-dark',
    'captionHelper' => '',
    'options' => [
        'class' => 'portlet light bordered'
    ],
    'tools' => [
    ],
    'actions' => [
        
    ]
]);
?>
    <?php Pjax::begin([ 'timeout' => 5000, 'id'=> 'history']) ?>
    <?= Datatables::widget([
        'id' => 'payment_datatable',
        'dataProvider' => $provider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'tableOptions' => ['id' => 'example1'],
        'hover' => true,
        'export' => false,
        'bordered' => false,
        'condensed' => true,
        'striped' => true,
        'responsive' => false,
        'layout' => '{items}',
        'clientOptions' => [
            "ajax" => 'query-history',
            "lengthMenu" => [[5,10,15,20,-1],[5,10,15,20,"All"]],
            "info" => true,
            "responsive" => true,
            "pageLength" => 20,
            "processing" => true,
            "columns" => [
                [ "data" => "order" ],
                [ "data" => "inv_num" ],
                [ "data" => "inv_date" ],
                [ "data" => "fee_amt" ],
                [ "data" => "disc_amt" ],
                [ "data" => "cardloss_amt" ],
                [ "data" => "total_paid" ],
                [ "data" => "createby" ],
                [ "data" => "action","bSortable"=>false]
            ],
            "columnDefs" => [
                [ "sClass" => "text-center", "targets" => [0,1,2,3,4,5,6,7,8] ],
                [ "sClass" => "dt-nowrap", "targets" => [] ]
            ],
            'fnRowCallback' => new \yii\web\JsExpression("function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $(nRow).attr( 'data-key' ,aData.inv_num);
                return nRow;
            } ")
        ],
        'columns' => [
            [
                'class' => 'msoft\widgets\grid\SerialColumn',
                'width' => '5%',
            ],
            [
                'header' => 'เลขที่',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '10%',
            ],
            [
                'header' => 'วันที่',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '15%',
            ],
            [
                'header' => 'ค่าบริการ',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '10%',
            ],
            [
                'header' => 'ส่วนลด',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '15%',
            ],
            [
                'header' => 'ค่าปรับบัตรหาย',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '10%',
            ],
            [
                'header' => 'เป็นเงิน',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '10%',
            ],
            [
                'header' => 'ผู้นำส่ง',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '10%',
            ],
            [
                'class' => 'msoft\widgets\ActionColumn',
                'noWrap' => TRUE,
                'width' => '15%',
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
<?= PortletBox::end(); ?>

<?php echo $this->render('modal'); ?>
<?php $this->registerJs(<<<JS
    $('#save-payment').click(function (e) {
        var keys = $('#payment_datatable').yiiGridView('getSelectedRows');
        if(keys.length == 0){
            Notify.warning('กรุณาเลือกรายการ');
            localStorage.clear();
        }else{
            swal({
                title: "บันทึกนำส่งเงินสด?",
                text: "ยืนยันการบันทึกข้อมูลนำส่งเงินสด",
                icon: "warning",
                buttons: true,
            }).then((willDelete) => {
                Loading.hide();
                if (willDelete) {
                    Loading.class();
                    $.ajax({
                        url: "savepayment",
                        data : {
                            id: keys
                        },
                        type: "POST",
                        success: function (data) {
                            $.pjax.reload({container: '#history'});
                            Notify.success('บันทึกข้อมูลเรียบร้อย');
                            Loading.hide();
                        },
                        error: function (xhr, status, error) {
                            Notify.error('error');
                            localStorage.clear();
                        }
                    });
                } 
            });
        } 
        
    });
    Notify = {
        success : function(massage){
            AppNotify.Show('success','fa fa-check','Success!',massage);
        },
        warning : function(massage){
            AppNotify.Show('warning','fa fa-info-circle','Warning!',massage);
        },
        error : function(massage){
            AppNotify.Show('danger','fa fa-remove','Error!',massage);
        },
    }
    Loading = {
        class : function(){
            $('.page-content').waitMe({
                effect : 'orbit',
                text: 'กำลังโหลดข้อมูล...',
                bg : 'rgba(255,255,255,0.7)',
                color : '#000',
                maxSize : '',
                textPos : 'vertical',
                fontSize : '18px',
                source : ''
            });
        },
        hide : function(){
            $('.page-content').waitMe('hide');
        }
    };
JS
);
?>