<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
?>
<ul class="nav nav-tabs " id="myTab5">
    <li id="tab_A">
        <a data-toggle="tab" href="#home5">
            <?= Html::encode('นำส่งเงินสด') ?> 
        </a>
    </li> 
    <li id="tab_B">
        <a data-toggle="tab" href="#home5">
            <?= Html::encode('ประวัติการนำส่งเงินสด') ?> 
        </a>
    </li>
</ul>
<?php
    $script = <<< JS
    $("#tab_A").click(function (e) {               
        window.location.replace("/payment/payment/index");
    });
     $("#tab_B").click(function (e) {               
        window.location.replace("/payment/payment/history");
    });
JS;
$this->registerJs($script);
?>