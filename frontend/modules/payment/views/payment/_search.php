<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payment\models\TbReceiptSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-receipt-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'rec_num') ?>

    <?= $form->field($model, 'trans_id') ?>

    <?= $form->field($model, 'fee_amt') ?>

    <?= $form->field($model, 'disc_amt') ?>

    <?= $form->field($model, 'cardloss_amt') ?>

    <?php // echo $form->field($model, 'total_paid') ?>

    <?php // echo $form->field($model, 'inv_num') ?>

    <?php // echo $form->field($model, 'print_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
