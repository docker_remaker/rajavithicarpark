<?php

use msoft\helpers\Html;
use msoft\widgets\Panel;
use msoft\widgets\Icon;
use msoft\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use msoft\widgets\Datatables;
use metronic\widgets\portlet\PortletBox;
use yii\data\ArrayDataProvider;
use yii\grid\GridViewAsset;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payment\models\TbReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
RegisterJS::regis(['sweetalert'],$this);
GridViewAsset::register($this);

$this->title = 'นำส่งเงินสด';
$this->params['breadcrumbs'][] = $this->title;
$classmidle = ['class' => 'kv-align-center kv-align-middle','style' => 'color:black'];
$this->registerCss('
    .btn-default {
        background : #fff !important;
    }
');
$provider = new ArrayDataProvider([
    'allModels' => [],
    'pagination' => [
        'pageSize' => 100,
    ],
]);
?>
<?= SwalAlert::widget(); ?>
<div class="tab-content">
<?php echo $this->render('_tab'); 
$this->registerJs('$("#tab_A").addClass("active");');
?>
<?= PortletBox::begin([
    'title' => '<span class="caption-subject bold uppercase">' . $this->title . '</span>',
    'icon' => 'fa fa-cube font-dark',
    'captionHelper' => '',
    'options' => [
        'class' => 'portlet light bordered'
    ],
    'tools' => [
    ],
    'actions' => [
        Html::button(Icon::show('save').'บันทึกนำส่งเงินสด', ['type' => 'button', 'class' => 'btn btn-success ', 'id' => 'save-payment','target' => '_blank'])
    ]
]);
?>
    <?php Pjax::begin([ 'timeout' => 5000, 'id'=> 'index']) ?>
    <?= Datatables::widget([
        'id' => 'payment_datatable',
        'dataProvider' => $provider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'tableOptions' => ['id' => 'example1'],
        'hover' => true,
        'bordered' => false,
        'condensed' => true,
        'export' => false,
        'striped' => true,
        'responsive' => false,
        'layout' => '{items}',
        'clientOptions' => [
            "ajax" => 'query',
            "lengthMenu" => [[5,10,15,20,-1],[5,10,15,20,"All"]],
            "info" => true,
            "responsive" => true,
            "pageLength" => 20,
            "processing" => true,
            "columns" => [
                [ "data" => "order" ],
                [ "data" => "rec_num" ],
                [ "data" => "licenceplate_no"],
                [ "data" => "gate3_datetime" ],
                [ "data" => "fee_amt" ],
                [ "data" => "disc_amt" ],
                [ "data" => "cardloss_amt" ],
                [ "data" => "total_paid" ],
                [ "data" => "checkbox","bSortable"=>false]
            ],
            "columnDefs" => [
                [ "sClass" => "text-center", "targets" => [0,1,2,3,4,5,6,7,8] ],
                [ "sClass" => "dt-nowrap", "targets" => [] ]
            ],
            'fnRowCallback' => new \yii\web\JsExpression("function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $(nRow).attr( 'data-key' ,aData.rec_num);
                return nRow;
            } ")
        ],
        'columns' => [
            [
                'class' => 'msoft\widgets\grid\SerialColumn',
                'width' => '5%',
            ],
            [
                'header' => 'เลขที่ใบเสร็จ',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '10%',
            ],
            [
                'header' => 'ทะเบียนรถ',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '10%',
            ],
            [
                'header' => 'วันที่',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '10%',
            ],
            [
                'header' => 'ค่าบริการ',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '15%',
            ],
            [
                'header' => 'ส่วนลด',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '15%',
            ],
            [
                'header' => 'ค่าปรับบัตรหาย',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '15%',
            ],
            [
                'header' => 'เป็นเงิน',
                'headerOptions' => $classmidle,
                'hAlign' => 'center',
                'width' => '15%',
            ],
            [
                'class' => 'msoft\widgets\grid\CheckboxColumn',
                'noWrap' => TRUE,
                'width' => '5%',
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
<?= PortletBox::end(); ?>

<?php echo $this->render('modal'); ?>
<?php $this->registerJs(<<<JS
    $('#save-payment').click(function (e) {
        
        var keys = $('#payment_datatable').yiiGridView('getSelectedRows');
        if(keys.length == 0){
            Notify.warning('กรุณาเลือกรายการ');
            localStorage.clear();
        }else{
            swal({
                title: "บันทึกนำส่งเงินสด?",
                text: "ยืนยันการบันทึกข้อมูลนำส่งเงินสด",
                icon: "warning",
                buttons: true,
            }).then((willDelete) => {
                Loading.hide();
                if (willDelete) {
                    Loading.class();
                    $.ajax({
                        url: "savepayment",
                        data : {
                            id: keys
                        },
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {
                            if(data != false){
                                $.pjax.reload({container: '#index'});
                                Notify.success('บันทึกข้อมูลเรียบร้อย');
                                Loading.hide();
                                window.open(data, "", "top=100,left=200,width=" + (screen.width - '400') + ",height=550,right=auto");
                            }else{
                                Notify.error('error');
                            }
                            
                        },
                        error: function (xhr, status, error) {
                            Notify.error('error');
                            localStorage.clear();
                        }
                    });
                } 
            });
        } 
        
    });
    Notify = {
        success : function(massage){
            AppNotify.Show('success','fa fa-check','Success!',massage);
        },
        warning : function(massage){
            AppNotify.Show('warning','fa fa-info-circle','Warning!',massage);
        },
        error : function(massage){
            AppNotify.Show('danger','fa fa-remove','Error!',massage);
        },
    }
    Loading = {
        class : function(){
            $('.page-content').waitMe({
                effect : 'orbit',
                text: 'กำลังโหลดข้อมูล...',
                bg : 'rgba(255,255,255,0.7)',
                color : '#000',
                maxSize : '',
                textPos : 'vertical',
                fontSize : '18px',
                source : ''
            });
        },
        hide : function(){
            $('.page-content').waitMe('hide');
        }
    };
JS
);
?>