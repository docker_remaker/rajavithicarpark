<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\card\models\TbCard */

$this->title = 'Update Tb Card: ' . $model->ids;
$this->params['breadcrumbs'][] = ['label' => 'Tb Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ids, 'url' => ['view', 'id' => $model->ids]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tb-card-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
