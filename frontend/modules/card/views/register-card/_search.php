<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\card\models\TbCardSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-card-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ids') ?>

    <?= $form->field($model, 'card_id') ?>

    <?= $form->field($model, 'card_type_id') ?>

    <?= $form->field($model, 'card_model_id') ?>

    <?= $form->field($model, 'card_owner_name') ?>

    <?php // echo $form->field($model, 'licenceplate_no') ?>

    <?php // echo $form->field($model, 'card_issuedate') ?>

    <?php // echo $form->field($model, 'card_expdate') ?>

    <?php // echo $form->field($model, 'card_status') ?>

    <?php // echo $form->field($model, 'createby') ?>

    <?php // echo $form->field($model, 'updateby') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
