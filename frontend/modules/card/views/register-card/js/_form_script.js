$('body').addClass('page-sidebar-closed');
$('.page-sidebar .page-sidebar-menu, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu').addClass('page-sidebar-menu-closed');
//Form
Project = {
    ApplyInputGRP : function(grpid){//เช็คว่าถ้าเป็นประเภทโครงการด้านบริหารวิชาการ ให้กรอกข้อมูลเพิ่มเติม
        //console.log(grpid);
        if(grpid === 'all'){//ให้กรอกข้อมูล
            $('div.subgrp-input-day').css('display', 'none');
            $('div.subgrp-input-date').css('display', 'none');
        }else if(grpid === 'day'){//ไม่ต้องกรอกข้อมูล
            $('div.subgrp-input-day').css('display', 'block');
            $('div.subgrp-input-date').css('display', 'none');
        }else if(grpid === 'date'){//ไม่ต้องกรอกข้อมูล
            $('div.subgrp-input-day').css('display', 'none');
            $('div.subgrp-input-date').css('display', 'block');
        }
    },
    AutoSave: function(){
        var frm = $('form[id="TbProject"]');
        //var Form = $('.tb-meeting-form').find("form")[0];
        var data = new FormData($(frm)[0]);
        console.log(data);
        $.ajax({
            "type":frm.attr('method'),
            "url": frm.attr('action'),//frm.attr('action')
            "data":data,
            "async": false,
            "dataType":"json",
            "success":function (result) {
                AppNotify.SaveCompleted();
            },
            "error":function (xhr, status, error) {
                AppNotify.Error(error);
            },
            contentType: false,
            cache: false,
            processData: false
        });
        return false;
    },
    initTabsActive: function(){
        //sessionStorage.removeItem('activeid');
        var sessionID = sessionStorage.getItem("activeid");
        if(sessionID !== null && (window.location.pathname === '/project/prj/update' || window.location.pathname === '/project/prj/sumup-project')){
            //Remove Class
            $('ul#tabs-left > li.active').removeClass('active');
            for (i = 0; i < 15; i++) { 
                $('div[id="tabs'+i+'"]').removeClass('active in');
            }
            //Add Class
            var tabsid = sessionID.replace("#", "");
            $('li[data-id="'+tabsid+'"]').addClass('active');
            $('div[id="'+tabsid+'"]').addClass('active in');
        }
    }
};
//
/*
$(function(){
    $('#TbProject input,select,textarea').on('change',function(e){
        Project.AutoSave();
    });
});*/
// $(function(){
//     //Loading
//     $('.page-container').waitMe({
//         effect : 'facebook',
//         text : 'Please wait...',
//         bg : 'rgba(255,255,255,0.7)',
//         color : '#000',
//         textPos : 'vertical',
//     });

//     $('ul#tabs-left > li >a').on('click',function(e){
//         if(window.location.pathname === '/project/prj/update' || window.location.pathname === '/project/prj/sumup-project'){
//             sessionStorage.setItem('activeid',$(this).attr('href'));
//         }
//     });
//     Project.initTabsActive();
// });

// window.onload = function () { $(".page-container").waitMe("hide"); }