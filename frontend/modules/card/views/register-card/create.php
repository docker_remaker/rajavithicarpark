<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\card\models\TbCard */

$this->title = 'เพิ่มข้อมูลบัตร';
$this->params['breadcrumbs'][] = ['label' => 'ลงทะเบียนบัตรจอดรถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-card-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
