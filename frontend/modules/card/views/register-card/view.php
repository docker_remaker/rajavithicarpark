<?php

use yii\helpers\Html;
use msoft\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\card\models\TbCard */

$this->title = $model->ids;
$this->params['breadcrumbs'][] = ['label' => 'Tb Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-card-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ids',
            'card_seq',
            'card_id',
            [
                'attribute' => 'card_type_id',
                'value' => @$model->cardtype->card_type_name
            ],
            [
                'attribute' => 'card_model_id',
                'value' => @$model->cardmodel->card_model_name
            ],
            'card_owner_name',
            'section_name',
            [
                'attribute' => 'company_id',
                'value' => @$model->company->company_name
            ],
            'licenceplate_no',
            'card_issuedate',
            'card_expdate',
            [
                'attribute' => 'card_status',
                'value' => @$model->cardstatus->card_status_name
            ],
            'lot_name',
             [
                'attribute' => 'lot_floor',
                'value' => @$model->cardfloor->lot_floor_name
            ],
        ],
    ]) ?>

</div>
