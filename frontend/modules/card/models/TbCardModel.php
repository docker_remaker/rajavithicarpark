<?php

namespace frontend\modules\card\models;

use Yii;

/**
 * This is the model class for table "tb_card_model".
 *
 * @property int $card_model_id รหัสรุ่นบัตร
 * @property string $card_model_name รุ่นบัตร
 */
class TbCardModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_card_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['card_model_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'card_model_id' => 'Card Model ID',
            'card_model_name' => 'Card Model Name',
        ];
    }
}
