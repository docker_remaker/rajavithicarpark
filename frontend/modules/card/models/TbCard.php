<?php

namespace frontend\modules\card\models;

use Yii;
use yii\db\ActiveRecord;
use metronic\components\DateConvert;
use msoft\behaviors\CoreMultiValueBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "tb_card".
 *
 * @property int $ids รหัสรายการ
 * @property string $card_id หมายเลขบัตร
 * @property int $card_type_id ประเภทบัตร
 * @property int $card_model_id รหัสรุ่นบัตร
 * @property string $card_owner_name ชื่อผู้ถือบัตร
 * @property string $licenceplate_no ทะเบียนรถ
 * @property string $card_issuedate วันเริ่มใช้บัตร
 * @property string $card_expdate หมดอายุ
 * @property int $card_status สถานะบัตร
 * @property int $createby สร้างโดย
 * @property string $createat สร้างเมื่อ
 * @property int $updateby ปรับปรุงโดย
 * @property string $updateat ปรับปรุงเมื่อ
 * @property string $lot_name ช่องจอดรถ
 * @property string $lot_floor ชั้นจอดรถ
 * @property int $contact_num
 * @property string $section_name
 * @property string $parking_type
 * @property string $parking_date
 * @property string $parking_mon
 * @property string $parking_tue
 * @property string $parking_wen
 * @property string $parking_thu
 * @property string $parking_fri
 * @property string $parking_sat
 * @property string $parking_sun
 */
class TbCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_card';
    }
    public function behaviors() {
        return[
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createby', 'updateby'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updateby',
                ],
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createat', 'updateat'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateat'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => CoreMultiValueBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['card_issuedate','card_expdate'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['card_issuedate','card_expdate'],
                ],
                'value' => function ($event) {
                    return DateConvert::phpDateTime2mysql($event->sender[$event->data]);
                },
            ],
            // [
            //     'class' => CoreMultiValueBehavior::className(),
            //     'attributes' => [
            //         ActiveRecord::EVENT_BEFORE_INSERT => ['card_id'],
            //     ],
            //     'value' => function ($event) {
            //         return substr($event->sender[$event->data],3);
            //     },
            // ],
            [
                    'class' => CoreMultiValueBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_AFTER_FIND => ['card_issuedate','card_expdate'],
                    ],
                    'value' => function ($event) {
                    return DateConvert::mysql2phpDateTime($event->sender[$event->data]);
                },
            ],
        ];
    }
    // public function beforeDelete()
    // {
    //     if (!parent::beforeDelete()) {
    //         return false;
    //     }
    //     \frontend\modules\gate\models\TbTransaction::deleteAll(['card_id' => $this->card_id]);
    //     \frontend\modules\gate\models\TbTransaction2::deleteAll(['card_id' => $this->card_id]);
    //     return true;
    // }
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if(!$insert) {
            if(isset($changedAttributes['card_id'])){
                \frontend\modules\gate\models\TbTransaction::updateAll(['card_id' => $this->card_id], ['card_id' => $changedAttributes['card_id']]);
                \frontend\modules\gate\models\TbTransaction2::updateAll(['card_id' => $this->card_id], ['card_id' => $changedAttributes['card_id']]);
            }
        }
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['card_type_id', 'card_model_id', 'card_status', 'createby', 'updateby', 'contact_num'], 'integer'],
            [['card_issuedate', 'card_expdate', 'createat', 'updateat'], 'safe'],
            [['card_id', 'card_owner_name', 'licenceplate_no', 'lot_name', 'section_name'], 'string', 'max' => 100],
            [['lot_floor'], 'string', 'max' => 50],
            [['parking_type'], 'string', 'max' => 10],
            [['card_id'], 'string','max' => 10],
            [['card_id'], 'unique', 'on' => 'create'],
            [['card_id'], 'unique', 'on' => 'update'],
            [['parking_sat'], 'unique', 'on' => 'saveday'],
            [['parking_date'], 'string', 'max' => 255],
            [['parking_mon','parking_tue', 'parking_wen', 'parking_thu', 'parking_fri', 'parking_sat', 'parking_sun'], 'string', 'max' => 1 ],
            [['card_id', 'card_type_id', 'card_model_id', 'card_owner_name','card_issuedate','card_status','parking_type'], 'required'],
        ];
    }
    public function scenarios()
    {
        $sn = parent::scenarios();
        $sn['create'] = ['section_id','company_id','parking_date','parking_mon','parking_tue', 'parking_wen', 'parking_thu', 'parking_fri', 'parking_sat', 'parking_sun','parking_type','card_seq','card_id','card_issuedate', 'card_owner_name', 'card_expdate', 'lot_name', 'createat', 'updateat','card_type_id', 'card_model_id', 'card_status', 'createby', 'updateby', 'lot_floor','licenceplate_no'];
        $sn['update'] = ['section_id','company_id','parking_date','parking_mon','parking_tue', 'parking_wen', 'parking_thu', 'parking_fri', 'parking_sat', 'parking_sun','parking_type','card_seq','card_id','card_issuedate', 'card_owner_name', 'card_expdate', 'lot_name', 'createat', 'updateat','card_type_id', 'card_model_id', 'card_status', 'createby', 'updateby', 'lot_floor','licenceplate_no'];
        return $sn;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ids' => 'รหัสรายการ',
            'card_seq' => 'ลำดับ',
            'card_id' => 'หมายเลขบัตร',
            'card_type_id' => 'ประเภทบัตร',
            'card_model_id' => 'รหัสรุ่นบัตร',
            'card_owner_name' => 'ชื่อผู้ถือบัตร',
            'licenceplate_no' => 'ทะเบียนรถ',
            'card_issuedate' => 'วันเริ่มใช้บัตร',
            'card_expdate' => 'หมดอายุ',
            'card_status' => 'สถานะบัตร',
            'createby' => 'สร้างโดย',
            'createat' => 'สร้างเมื่อ',
            'updateby' => 'ปรับปรุงโดย',
            'updateat' => 'ปรับปรุงเมื่อ',
            'lot_name' => 'ช่องจอดรถ',
            'section_id' => 'แผนก',
            'lot_floor' => 'ชั้นจอดรถ',
            'contact_num' => 'Contact Num',
            'section_name' => 'Section Name',
            'parking_type' => 'ประเภทการจอด',
            'parking_date' => 'ระบุวันที่',
            'parking_mon' => 'จันทร์',
            'parking_tue' => 'อังคาร',
            'parking_wen' => 'พุธ',
            'parking_thu' => 'พฤหัสบดี',
            'parking_fri' => 'ศุกร์',
            'parking_sat' => 'เสาร์',
            'parking_sun' => 'อาทิตย์',
            'company_id'  => 'สถาบัน',
        ];
    }
    public function getCardtype(){
        return $this->hasOne(TbCardType::className(),['card_type_id'=>'card_type_id']);
    }
    public function getCardmodel(){
        return $this->hasOne(TbCardModel::className(),['card_model_id'=>'card_model_id']);
    }
    public function getCardstatus(){
        return $this->hasOne(TbCardStatus::className(),['card_status'=>'card_status']);
    }
    public function getCardfloor(){
        return $this->hasOne(TbLotFloor::className(),['lot_floor'=>'lot_floor']);
    }
    public function getSection(){
        return $this->hasOne(TbSection::className(),['section_id'=>'section_id']);
    }
    public function getCompany(){
        return $this->hasOne(TbCompany::className(),['company_id'=>'company_id']);
    }


    public function getDefaultdate(){
        $year = date('Y')+543;
        return date('d/m/').$year.' '.date('h:i:s');
    }

    public function setValuedate($data){
        $value = [];
        if(!empty($this->parking_date)){
            $value = explode(",", $this->parking_date);
        }
        if(in_array($data, $value)){
            return 1;
        }else{
            return 0;
        }
        
    }
}

