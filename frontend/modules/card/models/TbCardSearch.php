<?php

namespace frontend\modules\card\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\card\models\TbCard;

/**
 * TbCardSearch represents the model behind the search form about `frontend\modules\card\models\TbCard`.
 */
class TbCardSearch extends TbCard
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ids', 'card_type_id', 'card_model_id', 'card_status', 'createby', 'updateby', 'contact_num'], 'integer'],
            [['card_id', 'card_owner_name', 'licenceplate_no', 'card_issuedate', 'card_expdate', 'createat', 'updateat', 'lot_name', 'lot_floor', 'section_name', 'parking_type', 'parking_date', 'parking_mon', 'parking_tue', 'parking_wen', 'parking_thu', 'parking_fri', 'parking_sat', 'parking_sun'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbCard::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ids' => $this->ids,
            'card_type_id' => $this->card_type_id,
            'card_model_id' => $this->card_model_id,
            'card_issuedate' => $this->card_issuedate,
            'card_expdate' => $this->card_expdate,
            'card_status' => $this->card_status,
            'createby' => $this->createby,
            'createat' => $this->createat,
            'updateby' => $this->updateby,
            'updateat' => $this->updateat,
            'contact_num' => $this->contact_num,
        ]);

        $query->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'card_owner_name', $this->card_owner_name])
            ->andFilterWhere(['like', 'licenceplate_no', $this->licenceplate_no])
            ->andFilterWhere(['like', 'lot_name', $this->lot_name])
            ->andFilterWhere(['like', 'lot_floor', $this->lot_floor])
            ->andFilterWhere(['like', 'section_name', $this->section_name])
            ->andFilterWhere(['like', 'parking_type', $this->parking_type])
            ->andFilterWhere(['like', 'parking_date', $this->parking_date])
            ->andFilterWhere(['like', 'parking_mon', $this->parking_mon])
            ->andFilterWhere(['like', 'parking_tue', $this->parking_tue])
            ->andFilterWhere(['like', 'parking_wen', $this->parking_wen])
            ->andFilterWhere(['like', 'parking_thu', $this->parking_thu])
            ->andFilterWhere(['like', 'parking_fri', $this->parking_fri])
            ->andFilterWhere(['like', 'parking_sat', $this->parking_sat])
            ->andFilterWhere(['like', 'parking_sun', $this->parking_sun]);

        return $dataProvider;
    }
}
