<?php

namespace frontend\modules\card\models;

use Yii;

/**
 * This is the model class for table "tb_section".
 *
 * @property int $section_id
 * @property string $section_name
 */
class TbSection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section_id'], 'required'],
            [['section_id'], 'integer'],
            [['section_name'], 'string', 'max' => 255],
            [['section_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'section_id' => 'Section ID',
            'section_name' => 'Section Name',
        ];
    }
}
