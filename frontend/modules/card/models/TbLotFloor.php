<?php

namespace frontend\modules\card\models;

use Yii;

/**
 * This is the model class for table "tb_lot_floor".
 *
 * @property int $lot_floor
 * @property string $lot_floor_name
 */
class TbLotFloor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_lot_floor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lot_floor_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lot_floor' => 'Lot Floor',
            'lot_floor_name' => 'Lot Floor Name',
        ];
    }
}
