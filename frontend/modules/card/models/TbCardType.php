<?php

namespace frontend\modules\card\models;

use Yii;

/**
 * This is the model class for table "tb_card_type".
 *
 * @property int $card_type_id รหัสประเภทบัตร
 * @property string $card_type_name ประเภทบัตร
 */
class TbCardType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_card_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['card_type_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'card_type_id' => 'Card Type ID',
            'card_type_name' => 'Card Type Name',
        ];
    }
}
