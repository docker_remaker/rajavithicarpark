<?php

namespace frontend\modules\card\controllers;

use Yii;
use frontend\modules\card\models\TbCard;
use frontend\modules\card\models\TbCardSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use msoft\helpers\Html;
use yii\filters\AccessControl;
use yii\db\Query;
use msoft\widgets\Icon;
/**
 * RegisterCardController implements the CRUD actions for TbCard model.
 */
class RegisterCardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TbCard models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new TbCardSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->pagination->pageSize = false;
        return $this->render('index', [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TbCard model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "ข้อมูลบัตรจอดรถ #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new TbCard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new TbCard();  
        $model->scenario = 'create'; 
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            $data = $request->post('TbCard',[]);
            if($request->isPost){
                $parking_date = [];
                foreach ($data as $key => $value) {
                    if( isset($data[$key]['parking_date']) && !empty($data[$key]['parking_date'])){
                        $parking_date[] = $key;

                    }
                }
                $model->parking_date = implode(",",$parking_date);
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "เพิ่มข้อมูลบัตรจอดรถ",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#index',
                    'title'=> "ลงทะเบียนบัตร",
                    'content'=>'<span class="text-success">เพิ่มข้อมูลบัตรจอดรถเรียบร้อยแล้ว</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                            Html::a('เพิ่มข้อมูล',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                
                return [
                    'title'=> "เพิ่มข้อมูลบัตรจอดรถ",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"]),
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                //$this->msgSuccess();
                return $this->redirect(['view', 'id' => $model->ids]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing TbCard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);    
         $model->scenario = 'update'; 

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            $data = $request->post('TbCard',[]);
            if($request->isPost){
                $parking_date = [];
                foreach ($data as $key => $value) {
                    if( isset($data[$key]['parking_date']) && !empty($data[$key]['parking_date'])){
                        $parking_date[] = $key;

                    }
                }
                $model->parking_date = implode(",",$parking_date);
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "แก้ไขข้อมูลบัตรจอดรถ #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default ','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#index',
                    'title'=> "ข้อมูลบัตรจอดรถ #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "แก้ไขข้อมูลบัตรจอดรถ #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default ','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->ids]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing TbCard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        \Yii::$app->getSession()->setFlash('success', 'Deleted!');
        return $this->redirect(['index']);
    }

    /**
     * Finds the TbCard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbCard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbCard::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    

    public function actionTest(){
        $query = (new Query())
        ->select([
            'tb_card.*',
            'tb_card_type.*',
            'tb_card_model.*',
            'tb_card_status.*',
            'tb_company.*',
        ])
        ->from('tb_card')
        ->leftJoin('tb_card_type','tb_card_type.card_type_id = tb_card.card_type_id')
        ->leftJoin('tb_card_model','tb_card_model.card_model_id = tb_card.card_model_id')
        ->leftJoin('tb_card_status','tb_card_status.card_status = tb_card.card_status')
        ->leftJoin('tb_company','tb_company.company_id = tb_card.company_id')
        ->all();
        $dataArray = [];
        foreach ($query as $key => $value) {
            $column = [];
            $column['order'] = $key+1;
            $column['licenceplate_no'] = $value['licenceplate_no'];
            $column['card_seq'] = $value['card_seq'];
            $column['card_id'] = $value['card_id'];
            $column['card_type_name'] = $value['card_type_name'];
            $column['card_owner_name'] = $value['card_owner_name'];
            $column['company'] = $value['company_name'];
            $column['section'] = $value['section_name'];
            $column['lot_name'] = $value['lot_name'];
            $column['card_issuedate'] = \metronic\components\DateConvert::convertToLogicalDateOnly($value['card_issuedate']);
            $column['card_expdate'] = \metronic\components\DateConvert::convertToLogicalDateOnly($value['card_expdate']);;
            $column['card_status_name'] = $value['card_status_name'];
            $column['action'] = 
            Html::a(Icon::show('eye-open', [], Icon::BSG),['/card/register-card/view','id' => $value['ids']],['class' => 'btn btn-xs btn-success tooltips','role' => 'modal-remote']).
            Html::a(Icon::show('pencil', [], Icon::BSG),['/card/register-card/update','id' => $value['ids']],['class' => 'btn btn-xs btn-primary tooltips','role' => 'modal-remote']).
            Html::aDelete(Icon::show('trash', [], Icon::BSG),false,[
                'data-url' => \yii\helpers\Url::to(['/card/register-card/delete','id' => $value['ids']]),
                'pjaxreload' => '#pjax-index',
                'class' => 'btn btn-xs btn-danger tooltips'
            ]);
            $dataArray[] = $column;
        }

        echo \yii\helpers\Json::encode(['data' => $dataArray]);
    }
}
