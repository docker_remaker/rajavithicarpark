<?php

namespace frontend\modules\gate\controllers;

use Yii;
use frontend\modules\gate\models\TbTransaction;
use frontend\modules\gate\models\TbTransaction2;
use frontend\modules\gate\models\TbCard;
use frontend\modules\gate\models\TbGate;
use frontend\modules\gate\models\TbCardPolicy;
use frontend\modules\gate\models\TbConfigCamera;
use frontend\modules\gate\models\TbConfigUi;
use frontend\modules\gate\models\ImgAttachment;
use frontend\modules\gate\models\TbReceipt;
use frontend\modules\gate\models\TbTransactionSearch;
use frontend\modules\gate\models\TbTransaction2Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\filters\AccessControl;
/**
 * TransactionController implements the CRUD actions for TbTransaction model.
 */
class TransactionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => ['*'],
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionUrlImage()
    {
        $request = Yii::$app->request;
        if($request->isPost){
            //$modelCamera = TbConfigCamera::findOne(['ip'=>$request->post('ip'),'gate_id'=>$request->post('gate_id')]);
                $model = new ImgAttachment();
                $model->gate_id = $request->post('gate_id');
                $model->path = "capture/".date("Y-m-d")."/";
                $model->name = $request->post('file_name').'.jpg';
            if ($model->save()) {
                return $model->id;
             }else{
                return false;
            }
        }
    }
    /**
     * Lists all TbTransaction models.
     * @return mixed
     */
    public function actionIndex($gate_id=null)
    {
        $this->deleteTemp();
        $modelGate = TbGate::findOne($gate_id);
        // if($gate_id==1){
        //     $searchModel = new TbTransactionSearch();
        //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //     //$dataProvider->query->andWhere('gate1_datetime >= curdate()');
        //     $dataProvider->query->orderBy(['trans_id'=>SORT_DESC]);
        // }elseif($gate_id==2){
        //     $searchModel = new TbTransactionSearch();
        //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //     $dataProvider->query->where('gate2_datetime is not null')->orderBy(['trans_id'=>SORT_DESC]);
        //     //->andWhere('gate2_datetime >= curdate()');
        // }elseif($gate_id==3){
        //     $searchModel = new TbTransaction2Search();
        //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //     $dataProvider->query->andWhere('gate3_datetime >= curdate()')->orderBy(['trans_id'=>SORT_DESC]);
        // }
        return $this->render('index', [
            'modelGate' => $modelGate,
        ]);
    }
    public function actionDataGate($gate_id=null)
    {
        if($gate_id==1){
            $model = TbTransaction::find()->orderBy(['trans_id'=>SORT_DESC])->all();
        }elseif($gate_id==2){
            $model = TbTransaction::find()->where('gate2_datetime is not null')->orderBy(['trans_id'=>SORT_DESC])->all();
        }elseif($gate_id==3){
            $model = TbTransaction2::find()->where('gate3_datetime >= curdate()')->orderBy(['gate3_datetime'=>SORT_DESC])->all();
        }
        $result = [];
        foreach ($model as $key => $value) {
            $arrData = [];
            $arrData['index'] = ($key+1);
            $arrData['card_id'] = $value['card_id'];
            $arrData['card_owner_name'] = $this->ownerName($value['card_id']);
            $arrData['licenceplate_no'] = !empty($value['licenceplate_no'])?$value['licenceplate_no']:'';
            $arrData['card_type_name'] = $this->cardType($value['card_id']);
            $arrData['gate1_datetime'] = Yii::$app->formatter->asDate($value['gate1_datetime'], 'php:d/m/Y H:i:s');
            $arrData['gate2_datetime'] = Yii::$app->formatter->asDate($value['gate2_datetime'], 'php:d/m/Y H:i:s');
            $arrData['gate3_datetime'] = Yii::$app->formatter->asDate($value['gate3_datetime'], 'php:d/m/Y H:i:s');
            $arrData['total_time'] = !empty($value['total_time'])?$value['total_time']:'';
            $arrData['trans_status'] = ($value['trans_status']==1)?'เข้าจอด':'ออกแล้ว';
            $arrData['action'] = Html::a('<span class="glyphicon glyphicon-trash"></span>',false, 
                [
                    'class' => 'btn btn-xs btn-danger tooltips',
                    'data-pjax' => false,
                    'title' => Yii::t('yii', 'Delete'),
                    //'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    //'data-method' => 'post',
                    'onclick' => 'btnDelete('.$value->trans_id.','.$gate_id.',"#dt_trans");',
                    'id' => 'btnDelete',

                ]);
            $result[] = $arrData;
        }
        echo Json::encode(['data' => $result]);
    }
    public function ownerName($card_id)
    {   
        $modelCard = TbCard::findOne(['card_id'=>$card_id]);
        if(!empty($modelCard)){
            return $modelCard->card_owner_name;
        }else{
            return '';
        }
    }
    public function cardType($card_id)
    {   
        $modelCard = TbCard::findOne(['card_id'=>$card_id]);
        if(!empty($modelCard)){
            return $modelCard->cardtype->card_type_name;
        }else{
            return '';
        }
    }
    public function actionData($card_id)
    {   
        $modelCard = TbCard::findOne(['card_id'=>$card_id]);
        if(!empty($modelCard)){
            return true;
        }else{
            return false;
        }
    }
    public function actionExpdate($card_id)
    {   
        $modelCard = TbCard::findOne(['card_id'=>$card_id]);
        if($modelCard['card_expdate'] > date("Y-m-d H:i:s")||$modelCard['card_expdate'] == null){
            return true;     
        }else{
            return false;
        }
    }
    public function actionPolicy($card_id,$gate_id)
    {   
        $modelCard = TbCard::findOne(['card_id'=>$card_id]);
        $modelPolicy = TbCardPolicy::findOne(['gate_id'=>$gate_id]);
        $arr_card_type_id = explode(',', $modelPolicy['card_type_id']);
        if(in_array($modelCard['card_type_id'], $arr_card_type_id)){
            return true;     
        }else{
            return false;
        }
    }

    public function actionState($card_id,$gate_id)
    {   
        $modelTran = TbTransaction::findOne(['card_id'=>$card_id]);
        if($gate_id==1){
            if(!empty($modelTran)){
                if($modelTran['trans_status'] == 2){
                    return true;  
                }else{
                    return false;  
                }
            }else{
                return true;
            }   
        }elseif($gate_id==2){
            if(!empty($modelTran)){
                if(empty($modelTran->gate2_datetime)){
                    return true;
                }else{
                    return false;
                }
            }else{
                return true;  
            }
        }
        
    }
    public function actionPark($card_id,$gate_id)
    {
        $modelTrans = TbTransaction::findOne(['card_id'=>$card_id,'trans_status'=>1]);
        if(!empty($modelTrans)){
            return true;
        }else{
            // $this->autoIncrement();
            // $modelCard = TbCard::findOne(['card_id'=>$card_id]);
            // $modelTrans = new TbTransaction();
            // $modelTrans->card_id = $card_id;
            // if($modelCard['card_type_id']!=1){
            //     $modelTrans->licenceplate_no = $modelCard['licenceplate_no'];
            // }
            // $modelTrans->gate1_datetime = date("Y-m-d H:i:s");
            // $modelTrans->gate2_datetime = date("Y-m-d H:i:s");
            // $modelTrans->gate3_datetime = date("Y-m-d H:i:s"); 
            // $modelTrans->trans_status =  1;
            // $modelTrans->save();
            return false;
        }
    }

    public function actionCondition($card_id,$gate_id,$img_ref=null)
    {   
        Yii::$app->response->format = Response::FORMAT_JSON;
        $modelCard = TbCard::findOne(['card_id'=>$card_id]);
        $modelGate = TbGate::findOne($gate_id);
        if(!empty($img_ref)){
            $modelImg = ImgAttachment::findOne($img_ref);
        }
        $titleOntime = "บันทึกข้อมูล";
        $titleOuttime = "บันทึกข้อมูล (ไม่มีสิทธิการใช้บัตรในวันนี้)";
        if($modelCard['card_type_id']==1){
            return [
               'type' => 'visitor',
               'title' => $titleOntime,
               'content' => $this->renderAjax('_modal_tran', [
                        'model' => new TbTransaction(),
                        'modelGate' => $modelGate,
                        'modelCard' => $modelCard,
                        'modelImg' => isset($modelImg)?$modelImg:null,
                ]),
               'footer' => false,
            ];
        }else{
            if($modelCard['parking_type']=='all'||$modelCard['parking_type']==null) {
                return [
                   'type' => 'memberOntime',
                   'title' => $titleOntime,
                   'content' => $this->renderAjax('_modal_tran', [
                            'model' => new TbTransaction(),
                            'modelGate' => $modelGate,
                            'modelCard' => $modelCard,
                            'modelImg' => isset($modelImg)?$modelImg:null,
                    ]),
                   'footer' => false,
                ];
            }elseif($modelCard['parking_type']=='date'){
                if($this->parking_date($card_id)=='OK'){
                    return [
                       'type' => 'memberOntime',
                       'title' => $titleOntime,
                       'content' => $this->renderAjax('_modal_tran', [
                                'model' => new TbTransaction(),
                                'modelGate' => $modelGate,
                                'modelCard' => $modelCard,
                                'modelImg' => isset($modelImg)?$modelImg:null,
                        ]),
                       'footer' => false,
                    ];
                }else{
                    return [
                       'type' => 'memberOuttime',
                       'title' => $titleOuttime,
                       'content' => $this->renderAjax('_modal_tran', [
                                'model' => new TbTransaction(),
                                'modelGate' => $modelGate,
                                'modelCard' => $modelCard,
                                'modelImg' => isset($modelImg)?$modelImg:null,
                        ]),
                       'footer' => false,
                    ];
                }
            }elseif($modelCard['parking_type']=='day'){    
                if($this->parking_day($card_id)=='OK'){
                    return [
                       'type' => 'memberOntime',  
                       'title' => $titleOntime,
                       'content' => $this->renderAjax('_modal_tran', [
                                'model' => new TbTransaction(),
                                'modelGate' => $modelGate,
                                'modelCard' => $modelCard,
                                'modelImg' => isset($modelImg)?$modelImg:null,
                        ]),
                       'footer' => false,
                    ];
                }else{
                    return [
                       'type' => 'memberOuttime',
                       'title' => $titleOuttime,
                       'content' => $this->renderAjax('_modal_tran', [
                                'model' => new TbTransaction(),
                                'modelGate' => $modelGate,
                                'modelCard' => $modelCard,
                                'modelImg' => isset($modelImg)?$modelImg:null,
                        ]),
                       'footer' => false,
                    ];
                }
            }
        }
    }
    public function actionBill($card_id,$gate_id,$img_ref=null)
    {   
        Yii::$app->response->format = Response::FORMAT_JSON;
        $modelTrans = TbTransaction::findOne(['card_id'=>$card_id,'trans_status'=>1]);
        $modelCard = TbCard::findOne(['card_id'=>$card_id]);
        $modelGate = TbGate::findOne($gate_id);
        if(!empty($img_ref)){
            $modelImg = ImgAttachment::findOne($img_ref);
        }
        $title = "บันทึกข้อมูล";
        $trans_id = $modelTrans->trans_id;
        if($modelCard['card_type_id']==1){
            return [
               'type' => 'visitor',
               'title' => $title,
               'content' => $this->renderAjax('_modal_bill', [
                        'modelTrans' => $modelTrans,
                        'modelGate' => $modelGate,
                        'modelCard' => $modelCard,
                        'modelImg' => isset($modelImg)?$modelImg:null,
                ]),
               'footer' => false,
            ];
        }else{
            if($modelCard['parking_type']=='all'||$modelCard['parking_type']==null) {
                if($this->parking_place($trans_id)=='NO'){
                    return [
                       'type' => 'member',
                       'trans_id' => $trans_id,
                       'title' => $title,
                       'content' => $this->renderAjax('_modal_bill', [
                            'modelTrans' => $modelTrans,
                            'modelGate' => $modelGate,
                            'modelCard' => $modelCard,
                            'modelImg' => isset($modelImg)?$modelImg:null,
                        ]),
                       'footer' => false,
                    ];
                }else{
                    return [
                       'type' => 'auto',
                       'trans_id' => $trans_id, 
                    ];
                }
            }elseif($modelCard['parking_type']=='date'){
                if($this->parking_date($card_id)=='OK'){
                    if($this->parking_place($trans_id)=='NO'){
                        return [
                           'type' => 'member',
                           'trans_id' => $trans_id,
                           'title' => $title,
                           'content' => $this->renderAjax('_modal_bill', [
                                    'modelTrans' => $modelTrans,
                                    'modelGate' => $modelGate,
                                    'modelCard' => $modelCard,
                                    'modelImg' => isset($modelImg)?$modelImg:null,
                            ]),
                           'footer' => false,
                        ];
                    }else{
                        return [
                           'type' => 'auto', 
                           'trans_id' => $trans_id,
                        ];
                    }    
                }else{
                    return [
                       'type' => 'member',
                       'trans_id' => $trans_id,
                       'title' => $title,
                       'content' => $this->renderAjax('_modal_bill', [
                            'modelTrans' => $modelTrans,
                            'modelGate' => $modelGate,
                            'modelCard' => $modelCard,
                            'modelImg' => isset($modelImg)?$modelImg:null,
                        ]),
                       'footer' => false,
                    ];
                }
            }elseif($modelCard['parking_type']=='day'){    
                if($this->parking_day($card_id)=='OK'){
                    if($this->parking_place($trans_id)=='NO'){
                        return [
                           'type' => 'member',
                           'trans_id' => $trans_id,
                           'title' => $title,
                           'content' => $this->renderAjax('_modal_bill', [
                                'modelTrans' => $modelTrans,
                                'modelGate' => $modelGate,
                                'modelCard' => $modelCard,
                                'modelImg' => isset($modelImg)?$modelImg:null,
                            ]),
                           'footer' => false,
                        ];
                    }else{
                        return [
                           'type' => 'auto', 
                           'trans_id' => $trans_id,
                        ];
                    }    
                }else{
                    return [
                       'type' => 'member',
                       'trans_id' => $trans_id,
                       'title' => $title,
                       'content' => $this->renderAjax('_modal_bill', [
                            'modelTrans' => $modelTrans,
                            'modelGate' => $modelGate,
                            'modelCard' => $modelCard,
                            'modelImg' => isset($modelImg)?$modelImg:null,
                        ]),
                       'footer' => false,
                    ];
                }
            }
        }
    }
    private function parking_date($card_id)
    {
        $sql = "SELECT f1('".$card_id."')";
        $result = Yii::$app->db->createCommand($sql)->queryScalar();
        return empty($result)?'NO':$result;
    }
    private function parking_day($card_id)
    {
        $sql = "SELECT f2('".$card_id."')";
        $result = Yii::$app->db->createCommand($sql)->queryScalar();
        return empty($result)?'NO':$result;
    }
    private function parking_place($trans_id)
    {
        $sql = "SELECT f3('".$trans_id."')";
        $result = Yii::$app->db->createCommand($sql)->queryScalar();
        return empty($result)?'NO':$result;
    }
    public function actionSave()
    {   $request = Yii::$app->request;
        if($request->isAjax){
            $this->autoIncrement();
            $data = $request->post('TbTransaction');
            $model = new TbTransaction();
            $model->card_id = $data['card_id'];
            $model->licenceplate_no = $data['licenceplate_no'];
            $model->gate1_datetime =  $request->post('gate1_datetime_hidden');
            $model->trans_status =  1;
            $model->save();
            if(!empty($request->post('img_ref'))){
                $modelImg = ImgAttachment::findOne($request->post('img_ref'));
                $modelImg->trans_id = $model->trans_id;
                $modelImg->save();
            }
            return 'success';
        }else{
            throw new NotFoundHttpException('Invalid Request');
        }
    }
    public function actionAutoSave()
    {   $request = Yii::$app->request;
        if($request->isPost){
            $gate_id = $request->post('gate_id');
            $card_id = $request->post('card_id');
            if($gate_id==1){
                $this->autoIncrement();
                $modelCard = TbCard::findOne(['card_id'=>$card_id]);
                $model = new TbTransaction();
                $model->card_id = $card_id;
                if($modelCard['card_type_id']!=1){
                    $model->licenceplate_no = $modelCard['licenceplate_no'];
                }
                $model->gate1_datetime =  date("Y-m-d H:i:s");
                $model->trans_status =  1;
                $model->save();
                if(!empty($request->post('img_ref'))){
                    $modelImg = ImgAttachment::findOne($request->post('img_ref'));
                    $modelImg->trans_id = $model->trans_id;
                    $modelImg->save();
                }
                return 'success'; 
            }elseif($gate_id==2){
                $modelTrans = TbTransaction::findOne(['card_id'=>$card_id,'trans_status'=>1]);
                if(!empty($modelTrans)){
                    $modelTrans->gate2_datetime =  date("Y-m-d H:i:s");
                    $modelTrans->save();
                    if(!empty($request->post('img_ref'))){
                        $modelImg = ImgAttachment::findOne($request->post('img_ref'));
                        $modelImg->trans_id = $modelTrans->trans_id;
                        $modelImg->save();
                    }
                    return 'success'; 
                }else{
                    $this->autoIncrement();
                    $modelCard = TbCard::findOne(['card_id'=>$card_id]);
                    $modelTrans = new TbTransaction();
                    $modelTrans->card_id = $card_id;
                    if($modelCard['card_type_id']!=1){
                        $modelTrans->licenceplate_no = $modelCard['licenceplate_no'];
                    }
                    $modelTrans->gate1_datetime =  date("Y-m-d H:i:s");
                    $modelTrans->gate2_datetime =  date("Y-m-d H:i:s");
                    $modelTrans->trans_status =  1;
                    $modelTrans->save();
                    if(!empty($request->post('img_ref'))){
                        $modelImg = ImgAttachment::findOne($request->post('img_ref'));
                        $modelImg->trans_id = $modelTrans->trans_id;
                        $modelImg->save();
                    }
                    return 'success'; 
                }
            }
        }else{
            throw new NotFoundHttpException('Invalid Request');
        }
    }
    public function actionSaveBill()
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $data = $request->post('TbTransaction');
                $model = TbTransaction::findOne($request->post('trans_id'));
                $model->gate3_datetime =  $request->post('gate3_datetime_hidden');
                $model->total_time = $data['total_time'];
                $model->fee_amt = $data['fee_amt'];
                $model->disc_amt = $request->post('disc_amt');
                $model->total_amt = $data['total_amt'];
                $model->total_paid = $data['total_paid'];
                $model->trans_status = 2;
                $model->save();
                if(($model->total_paid)>0){
                    $modelReceipt = new TbReceipt();
                    $modelReceipt->trans_id = $model->trans_id;
                    $modelReceipt->fee_amt =  $model->fee_amt;
                    $modelReceipt->disc_amt =  $model->disc_amt;
                    $modelReceipt->total_paid = $model->total_paid;
                    $modelReceipt->print_status = 1;
                    $modelReceipt->save();
                }else{
                    $modelReceipt = new TbReceipt();
                    $modelReceipt->trans_id = $model->trans_id;
                    $modelReceipt->fee_amt =  $model->fee_amt;
                    $modelReceipt->disc_amt =  $model->disc_amt;
                    $modelReceipt->total_paid = 0;
                    $modelReceipt->print_status = 1;
                    $modelReceipt->save();
                }
                if(!empty($request->post('img_ref'))){
                    $modelImg = ImgAttachment::findOne($request->post('img_ref'));
                    $modelImg->trans_id = $model->trans_id;
                    $modelImg->save();
                }
                $modelTrans2 = new TbTransaction2();
                $modelTrans2->setAttributes($model->getAttributes());
                $modelTrans2->save();
                $model->delete();
                $transaction->commit();
                return 'success';
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }else{
            throw new NotFoundHttpException('Invalid Request');
        }
    }
    public function actionCheckBill()
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model = TbTransaction::findOne($request->post('trans_id'));
                if(empty($model->gate3_datetime)){
                     $model->gate3_datetime = date("Y-m-d H:i:s"); 
                }
                $model->total_time = Yii::$app->db->createCommand("SELECT func_total_time('".$model->gate1_datetime."','".date("Y-m-d H:i:s")."')")->queryScalar();
                $model->trans_status = 2;
                $model->save();
                if(!empty($request->post('img_ref'))){
                    $modelImg = ImgAttachment::findOne($request->post('img_ref'));
                    $modelImg->trans_id = $model->trans_id;
                    $modelImg->save();
                }
                $modelTrans2 = new TbTransaction2();
                $modelTrans2->setAttributes($model->getAttributes());
                $modelTrans2->save();
                $model->delete();
                $transaction->commit();
                return 'success';
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }else{
            throw new NotFoundHttpException('Invalid Request');
        }
    }
    public function autoIncrement() {
        $maxTrans_in = TbTransaction::find()->max('trans_id');
        $maxTrans_out = TbTransaction2::find()->max('trans_id');
        if(empty($maxTrans_in)&&empty($maxTrans_out)){
            $setAuto ="ALTER TABLE tb_transaction AUTO_INCREMENT = 1;";
        }elseif($maxTrans_out>$maxTrans_in) {
            $nextId = $maxTrans_out+1;
            $setAuto ="ALTER TABLE tb_transaction AUTO_INCREMENT = $nextId;";
        }elseif($maxTrans_out<$maxTrans_in){
            $nextId = $maxTrans_in+1;
            $setAuto ="ALTER TABLE tb_transaction AUTO_INCREMENT = $nextId;";
        }else{
            $nextId = $maxTrans_in+1; 
            $setAuto ="ALTER TABLE tb_transaction AUTO_INCREMENT = $nextId;";
        }
        Yii::$app->db->createCommand($setAuto)->query();
    }
    /**
     * Displays a single TbTransaction model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "TbTransaction #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new TbTransaction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new TbTransaction();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new TbTransaction",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new TbTransaction",
                    'content'=>'<span class="text-success">Create TbTransaction success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new TbTransaction",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->trans_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing TbTransaction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update TbTransaction #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "TbTransaction #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update TbTransaction #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->trans_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing TbTransaction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$gate_id)
    {
        if($gate_id==1||$gate_id==2){
            $model = $this->findModel($id);
        }elseif($gate_id==3){
            $model = TbTransaction2::findOne($id);
            $modelReceipt = TbReceipt::findOne(['trans_id'=>$id]);
            if(!empty($modelReceipt)){
                $modelReceipt->delete();
            }
        }
        $modelImgs = ImgAttachment::findAll(['trans_id'=>$id]);
        if(!empty($modelImgs)){
            foreach ($modelImgs as $modelImg) {
                @unlink($modelImg['path'].$modelImg['name']);
            }
            ImgAttachment::deleteAll('trans_id = :trans_id', [':trans_id' => $id]);
        }
        $model->delete();
        return 'success';
        //return $this->redirect(['index','gate_id'=>$gate_id]);
    }
    public function deleteTemp()
    {   
        $query_temp = ImgAttachment::find()->select('id')->where('createat <= (DATE_SUB(curdate(), INTERVAL 1 DAY))')->andWhere(['trans_id' => null])->asArray()->all();
        if(!empty($query_temp)){
            foreach ($query_temp as $key_temp) {
               @unlink($key_temp['path'].$key_temp['name']);
               ImgAttachment::deleteAll('id = :id', [':id' => $key_temp['id']]); 
            }
        }
    }
    public function actionDeleteImg($img_ref)
    {   
        $modelImg = ImgAttachment::findOne(['id'=>$img_ref,'trans_id'=>null]);
        if(!empty($modelImg)){
            @unlink($modelImg['path'].$modelImg['name']);
            $modelImg->delete();
        }
    }

    /**
     * Finds the TbTransaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbTransaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbTransaction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
