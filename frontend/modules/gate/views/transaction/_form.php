<?php

use yii\helpers\Html;
use msoft\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\gate\models\TbTransaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-transaction-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);?>

    <?= $form->field($model, 'card_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'licenceplate_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gate1_datetime')->textInput() ?>

    <?= $form->field($model, 'gate2_datetime')->textInput() ?>

    <?= $form->field($model, 'gate3_datetime')->textInput() ?>

    <?= $form->field($model, 'total_min')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fee_amt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'disc_type')->textInput() ?>

    <?= $form->field($model, 'disc_amt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'disc_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_amt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_paid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'invoice_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'trans_status')->textInput() ?>

    <?= $form->field($model, 'createby')->textInput() ?>

    <?= $form->field($model, 'updateby')->textInput() ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group" style="text-align:right;">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>
