<?php

namespace frontend\modules\gate\models;

use Yii;

/**
 * This is the model class for table "tb_card_status".
 *
 * @property integer $card_status
 * @property string $card_status_name
 */
class TbCardStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_card_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['card_status_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'card_status' => 'Card Status',
            'card_status_name' => 'Card Status Name',
        ];
    }
}
