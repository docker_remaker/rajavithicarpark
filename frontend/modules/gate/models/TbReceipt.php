<?php

namespace frontend\modules\gate\models;

use Yii;

/**
 * This is the model class for table "tb_receipt".
 *
 * @property integer $rec_num
 * @property integer $trans_id
 * @property string $fee_amt
 * @property string $disc_amt
 * @property string $cardloss_amt
 * @property string $total_paid
 * @property integer $inv_num
 * @property integer $print_status
 */
class TbReceipt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_receipt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trans_id', 'inv_num', 'print_status'], 'integer'],
            [['fee_amt', 'disc_amt', 'cardloss_amt', 'total_paid'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rec_num' => 'เลขที่ใบเสร็จรับเงิน',
            'trans_id' => 'เลขที่รายการ',
            'fee_amt' => 'ค่าบริการจอดรถยนต์',
            'disc_amt' => 'ส่วนลด',
            'cardloss_amt' => 'ค่าปรับบัตรหาย',
            'total_paid' => 'รวมเป็นเงิน',
            'inv_num' => 'เลขที่การนำสั่งเงินสด',
            'print_status' => 'สถานะการสั่งพิมพ์',
        ];
    }
}
