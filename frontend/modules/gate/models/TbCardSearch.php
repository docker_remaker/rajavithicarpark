<?php

namespace frontend\modules\gate\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\gate\models\TbCard;

/**
 * TbCardSearch represents the model behind the search form about `frontend\modules\gate\models\TbCard`.
 */
class TbCardSearch extends TbCard
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ids', 'card_type_id', 'card_model_id', 'card_status', 'createby', 'updateby'], 'integer'],
            [['card_id', 'card_owner_name', 'licenceplate_no', 'card_issuedate', 'card_expdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbCard::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ids' => $this->ids,
            'card_type_id' => $this->card_type_id,
            'card_model_id' => $this->card_model_id,
            'card_issuedate' => $this->card_issuedate,
            'card_expdate' => $this->card_expdate,
            'card_status' => $this->card_status,
            'createby' => $this->createby,
            'updateby' => $this->updateby,
        ]);

        $query->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'card_owner_name', $this->card_owner_name])
            ->andFilterWhere(['like', 'licenceplate_no', $this->licenceplate_no]);

        return $dataProvider;
    }
}
