<?php

namespace frontend\modules\gate\models;

use Yii;

/**
 * This is the model class for table "tb_gate".
 *
 * @property integer $gate_id
 * @property string $gate_name
 * @property string $gate_type
 */
class TbGate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_gate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gate_name'], 'string', 'max' => 100],
            [['gate_type'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gate_id' => 'รหัสประตู',
            'gate_name' => 'ชื่อประตู',
            'gate_type' => 'ประเภทประตู',
        ];
    }
    public function getRelay(){
        return $this->hasOne(TbConfigRelay::className(),['gate_id'=>'gate_id']);
    }
    public function getCamera(){
        return $this->hasMany(TbConfigCamera::className(),['gate_id'=>'gate_id']);
    }
}
