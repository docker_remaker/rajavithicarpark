<?php

namespace frontend\modules\gate\models;

use Yii;

/**
 * This is the model class for table "tb_card_type".
 *
 * @property integer $card_type_id
 * @property string $card_type_name
 */
class TbCardType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_card_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['card_type_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'card_type_id' => 'รหัสประเภทบัตร',
            'card_type_name' => 'ประเภทบัตร',
        ];
    }
}
