<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\MetronicAsset;
use frontend\assets\AppAsset;
use metronic\helpers\RegisterJS;

MetronicAsset::register($this);
AppAsset::register($this);
RegisterJS::regis(['waitme'],$this);
$bodyOptions = (isset($page) && in_array($page,['login','error'])) ? ['class'=>'login'] : ['class' => 'page-sidebar-closed-hide-logo page-md page-header-fixed page-sidebar-fixed page-container-bg page-sidebar-closed'];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <link rel="shortcut icon" type="image/x-icon" href="<?= Yii::getAlias('@web') ?>/images/logo/icon_shotcut.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for blank page layout" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <?= Html::csrfMetaTags() ?>
    <title><?php echo Html::encode(!empty($this->title) ? strtoupper($this->title) . ' | '.Yii::$app->params['webTitle'] : Yii::$app->params['webTitle']); ?></title>
    <?php $this->head() ?>
</head>
<?=Html::beginTag('body',$bodyOptions); ?>

    <?php $this->beginBody() ?>

    <?=$content;?>

    <?php $this->endBody() ?>

<?=Html::endtag('body'); ?>

</html>
<?php $this->endPage() ?>