<?php 
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <?= Breadcrumbs::widget([
                'options' => [
                    'class' => 'page-breadcrumb'
                ],
                'itemTemplate' => "<li>{link}<i class='fa fa-circle'></i></li>\n",
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
        <?php /*
        <h1 class="page-title"><?= $this->title; ?>
            <small></small>
</h1>*/?>
        <p></p>
        <?= $content; ?>

    </div>
</div>