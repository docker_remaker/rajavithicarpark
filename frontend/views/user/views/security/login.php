<?php

/*
 * This file is part of the msoft project.
 *
 * (c) msoft project <http://github.com/msoft>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use msoft\user\widgets\Connect;
use msoft\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var msoft\user\models\LoginForm $model
 * @var msoft\user\Module $module
 */
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@metronic/assets');
$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<div class="logo">
    <?= Html::a(Html::img('@web/images/logo/ap-logo.png?v1.0',['alt' => '']), ['/'],[]); ?>
</div>
<div class="content">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validateOnBlur' => false,
        'validateOnType' => false,
        'validateOnChange' => false,
]) ?>
<h3 class="form-title font-green">Sign In</h3>

    <?php if ($module->debug): ?>
        <?= $form->field($model, 'login', [
            'inputOptions' => [
                'autofocus' => 'autofocus',
                'class' => 'form-control',
                'tabindex' => '1']])->dropDownList(LoginForm::loginList());
        ?>

    <?php else: ?>

        <?= $form->field($model, 'login',['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']])->textInput([
            'placeholder' => 'Username'
        ])->label(false);
        ?>

    <?php endif ?>

    <?php if ($module->debug): ?>
        <div class="alert alert-warning">
            <?= Yii::t('user', 'Password is not necessary because the module is in DEBUG mode.'); ?>
        </div>
    <?php else: ?>
        <?= $form->field($model,
            'password',
            ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])
            ->passwordInput(['placeholder' => 'Password'])
            ->label(false) ?>
    <?php endif ?>

    <div class="form-actions">
        <?= Html::submitButton(
            Yii::t('user', 'Sign in'),
            ['class' => 'btn green uppercase', 'tabindex' => '4']
        ) ?>
        <?= (
            $module->enablePasswordRecovery ? Html::a(Yii::t('user', 'Forgot password?'),['/user/recovery/request'],['tabindex' => '5']
        ) : '') 
        ?>
        <?php // $form->field($model, 'rememberMe')->label('Remember',['class' => 'rememberme check mt-checkbox mt-checkbox-outline'])->checkbox(['tabindex' => '3']) ?>
    </div>
    <div class="login-options">
         <p class="text-center">
            <?= Html::a('ทางเข้า 1', ['/gate/transaction/index','gate_id'=>1],['class'=> 'btn btn-success']) ?>
            <?= Html::a('ทางเข้า 2', ['/gate/transaction/index','gate_id'=>2],['class'=> 'btn btn-success']) ?>
            <?= Html::a('ทางออก 1', ['/gate/transaction/index','gate_id'=>3],['class'=> 'btn btn-danger']) ?>
        </p>
        <?php if ($module->enableConfirmation): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
            </p>
        <?php endif ?>
        <?= Connect::widget([
            'baseAuthUrl' => ['/user/security/auth'],
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
