var s2options = {"themeCss": ".select2-container--krajee", "sizeCss": "input-sm", "doReset": true, "doToggle": false, "doOrder": false};

function initSt2(dt,s2id,index,placeholder) {
    dt.api().columns(index).every(function () {
        var column = this;
        var select = $('<select id="'+s2id+'" style="width: 100%;" class="select2"><option value="" >All</option></select>')
                .appendTo($(column.header()))
                .on("change", function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                            );
                    column
                            .search(val ? "^" + val + "$" : "", true, false)
                            .draw();
                });
        column.data().unique().sort().each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>')
        });
    });
    jQuery.when(jQuery("#"+s2id).select2({"allowClear": true, "theme": "krajee", "width": "100%", "placeholder": placeholder, "language": "th-TH"})).done(initS2Loading(s2id, "s2options"));
    $("#"+s2id + ",span.select2-container--krajee").addClass("input-sm");
}